# File to evaluate the performance of the GAN architectures
import pandas as pd
import numpy as np
import os
import argparse
import tensorflow as tf
from dataManagement import Data, preprocessData_model,LabelledData
from networks import discriminator,ConditionalDiscriminator
from training import parseFlags_tuple
from sklearn.neighbors import NearestNeighbors
import numpy.linalg as LA
import sklearn.manifold as skman
import matplotlib.pyplot as plt
import time
import seaborn as sns
from mpl_toolkits.mplot3d import Axes3D
################################
from scipy.spatial.distance import jensenshannon as js
from scipy.stats import chisquare as chisq
from scipy import stats as stats
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from xgboost.sklearn import XGBClassifier
from sklearn.metrics import confusion_matrix,  accuracy_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score as auroc
from xgboost import plot_importance
###################################

def str_to_bool(string):
    if string in ['Yes', 'yes', 'y', 'Y']:
        return True

    elif string in ['No', 'no', 'n', 'N']:
        return False

    else:
        raise ValueError("Invalid string passed to create a boolean value")
##############

parser = argparse.ArgumentParser()

parser.add_argument("-fake_data_path", type=str, help="path to fake data",
                    default='../generated_data/CondCramer_C_pca_credit_SMOTE_epochs_400_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_10:58:40,18M2019-08-18_genM_5/final_generated_data.csv')
# PATHS TO THE ENCODED DATA FOR FEATURE SPACE COMPARISONS
parser.add_argument("--train_encoded_path",type = str, help = "path to the encoded training data",
                    default = "../generated_data/CondCramer_C_PAYSIM_raw_epochs_40_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_00:34:20,07M2019-08-07_genM_5/realEncoded_iter695910.csv.gz")
parser.add_argument("--fake_encoded_path",type = str, help = "path to the encoded generated data",
                    default = "../generated_data/CondCramer_C_PAYSIM_raw_epochs_40_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_00:34:20,07M2019-08-07_genM_5/fakeEncoded_iter695910.csv.gz")

parser.add_argument("--save_results", type=str, help="Whether or not the results are saved", default="Y")
parser.add_argument("--root_path", type=str,help="Path to where we are", default="../")
parser.add_argument("--model_path",type = str, help = "Model path",
                    default = "../models/CondCramer_C_PAYSIM_raw_epochs_40_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_00:34:20,07M2019-08-07_genM_5/FINAL_model.ckpt")
parser.add_argument("--use_case", type= str, help = "what is the data being used for",default="pca_credit")
parser.add_argument("--disc_hyper",type = list,
                    help="Hyperparameters the saved discriminator [k,\"discDims_tuple\",output,ncross]",
                    default=[5,"(128,128)",64,0])

parser.add_argument("--n_points", type = int,
                     help="number of points to generate",default =12000) #default is 12000

args = parser.parse_args()

fake_data_path = args.fake_data_path
train_encoded_path = args.train_encoded_path
fake_encoded_path = args.fake_encoded_path
save_results = str_to_bool(args.save_results)
root_path = args.root_path
model_path = args.model_path
disc_hyper = args.disc_hyper
use_case = args.use_case
n_points = args.n_points

evaluation_name = "CondCramer_C_pca_credit_SMOTE_epochs_400_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_10:58:40,18M2019-08-18_genM_5"
# assert evaluation_name in model_path
assert "FINAL" not in evaluation_name
# assert use_case in ["PNR", "PAYSIM","PAYSIM_raw","PAYSIM_features","PAYSIM_balanced",
#                     "PAYSIM_fraud","PAYSIM_GAN","PAYSIM_features_fraud","PAYSIM_step",
#                     "PAYSIM_SMOTE_raw","PAYSIM_raw_balanced"]
# TODO: make this not hardcoded
if use_case == "PNR":
    real_data_path = os.path.join(root_path,"data/processedAirplane/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/processedAirplane/test.csv")
    enc_dims = 337
elif use_case == "PAYSIM_raw":
    real_data_path = os.path.join(root_path,"data/Paysim_raw/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim_raw/test.csv")
    enc_dims = 15

elif use_case == "PAYSIM_SMOTE_raw":
    real_data_path = os.path.join(root_path,"data/PAYSIM_raw/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/PAYSIM_raw/test.csv")
    enc_dims = 15

elif use_case == "PAYSIM":
    real_data_path = os.path.join(root_path,"data/Paysim/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim/test.csv")
    enc_dims = 15


elif use_case == "PAYSIM_balanced":
    real_data_path = os.path.join(root_path,"data/Paysim/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim/test.csv")
    enc_dims = 15

elif use_case == "PAYSIM_raw_balanced":
    real_data_path = os.path.join(root_path,"data/Paysim_raw_balanced/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim_raw_balanced/test.csv")
    enc_dims = 15

elif use_case == "PAYSIM_GAN":
    real_data_path = os.path.join(root_path,"data/Paysim/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim/test.csv")
    enc_dims = 15

elif use_case == "PAYSIM_features":
    real_data_path = os.path.join(root_path,"data/Paysim_features/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim_features/test.csv")
    enc_dims = 15

elif use_case == "PAYSIM_features_fraud":
    real_data_path = os.path.join(root_path,"data/Paysim_features_fraud/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim_features_fraud/test.csv")
    enc_dims = 15


elif use_case == "PAYSIM_fraud":
    real_data_path = os.path.join(root_path,"data/Paysim_fraud/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim_fraud/test.csv")
    enc_dims = 11

elif use_case == "PAYSIM_raw_fraud":
    real_data_path = os.path.join(root_path,"data/Paysim_raw_fraud/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim_raw_fraud/test.csv")
    enc_dims = 11

elif use_case == "PAYSIM_raw_normal":
    real_data_path = os.path.join(root_path,"data/Paysim_raw_normal/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim_raw_normal/test.csv")
    enc_dims = 11

elif use_case == "PAYSIM_sub_raw_normal":
    real_data_path = os.path.join(root_path,"data/Paysim_sub_raw_normal/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim_sub_raw_normal/test.csv")
    enc_dims = 11

elif use_case == "PAYSIM_clean_normal":
    real_data_path = os.path.join(root_path,"data/Paysim_normal_clean/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/Paysim_normal_clean/test.csv")
    enc_dims = 11


elif use_case == 'pca_credit':
    real_data_path = os.path.join(root_path,"data/pca_credit/test_evaluation.csv")
    real_data_path_encode = os.path.join(root_path,"data/pca_credit/test.csv")
    enc_dims = 30


else:

    raise ValueError("Wrong use case")


class evaluator(object):

    def __init__(self,cols_dict):

        # First we define the columns to be used in each of the situations

        self.cols_dict = cols_dict

        # Get the columns from the appropriate dictionary and check that they are correct
        self.use_cols = [item for item in self.cols_dict]
        self.cat_cols = [item for item in self.cols_dict if self.cols_dict[item] == 'cat']
        self.num_cols = [item for item in self.cols_dict if self.cols_dict[item] == 'num']


    def column_tests(self, df_real, df_fake,evaluation_name):

        print("Running the univariate tests")
        assert (df_real.columns == self.use_cols).all and (df_fake.columns == self.use_cols).all

        if 'isFraud' in self.cols_dict and use_case not in ['PAYSIM_raw_fraud','PAYSIM_raw_normal','PAYSIM_fraud']:
            print("paysim")

            names = ['normal','fraud']
            label_col = 'isFraud'
            labels = {1:1,0:0}

        elif use_case=='PNR':
            print("PNR")
            names = ['business', 'leisure']
            label_col = 'BL'
            labels = {1:'B',0:'L'}

        elif use_case == 'pca_credit':

            print('pca_credit')
            names = ['normal','fraud']
            label_col = 'Class'
            labels = {1:1,0:0}


        else:
            print("Only one class being learned")
            names = None
            label_col = None
            labels = None


        if ('isFraud' in self.cols_dict and use_case not in ['PAYSIM_raw_fraud','PAYSIM_raw_normal','PAYSIM_fraud',"PAYSIM_clean_normal"]) or use_case in ["PNR","pca_credit"]:
        # Now this is essentially just checking out some summary statistics of the 2 distributions
        # This entails plotting histograms and summary stats for
        # the continuous variables and doing class counts for the categorical ones
            with open(os.path.join(root_path, "textfiles", "{}.txt".format(evaluation_name)), 'w') as outfile:
                for j in range(2):
                    for c in self.use_cols:
                        if self.cols_dict[c] == 'cat':

                            if save_results:
                                summary_real = df_real.loc[df_real[label_col]==labels[j], c].astype(str).value_counts(normalize=True).head(15)
                                summary_fake = df_fake.loc[df_fake[label_col]==labels[j], c].astype(str).value_counts(normalize=True).head(15)

                                if j==0:
                                    a = df_real[c].astype(str).value_counts(normalize=False)
                                    b = df_fake[c].astype(str).value_counts(normalize=False)

                                    nums = min(a.shape[0],b.shape[0],5)

                                    chisqs, p = chisq( a.head(nums), b.head(nums) )
                                    # Print to the text file
                                    outfile.write(
                                        "\nThe p-value for the chisquared test is {}\n".format(p))
                                outfile.write("\nSummary of real data for feature {} for label {} \n".format(c,labels[j]))
                                summary_real.to_string(outfile)
                                outfile.write("\nSummary of fake data for feature {} for label {} \n".format(c,labels[j]))
                                summary_fake.to_string(outfile)

                            else:

                                print(df_real[c].astype(str).value_counts(normalize=True).head(15))
                                print(df_fake[c].astype(str).value_counts(normalize=True).head(15))

                        else:

                            if save_results:
                                summary_real = df_real.loc[df_real[label_col]==labels[j], c].astype(np.float).describe()
                                summary_fake = df_fake.loc[df_fake[label_col]==labels[j], c].astype(np.float).describe()

                                T,p = stats.mannwhitneyu(np.sort(df_real[c].values),np.sort(df_fake[c].values))
                                jsd = js(df_real[c].values,df_fake[c].values)
                                # jsd_split = js(df_real.loc[df_real[label_col]==labels[j], c].values[:-1], df_fake.loc[df_fake[label_col]==labels[j],c].values)

                                outfile.write("\nThe js divergence between the two is [%.6f] \n" % jsd)
                                # outfile.write("\nThe js divergence between for category" + labels[j] + " is [%.6f] \n" % jsd_split)
                                outfile.write("\nT count and p-value of Mann-Whitney U test for feature {} are: [%.0f] [%.6f] \n".format(c) % (T, p))
                                outfile.write("\nSummary of real data for feature {} in category {} \n".format(c,labels[j]))
                                summary_real.to_string(outfile)
                                outfile.write("\nSummary of fake data for feature {} in category {} \n".format(c,labels[j]))
                                summary_fake.to_string(outfile)

                            else:
                                print(df_real[c].astype(np.float).describe())
                                print(df_fake[c].astype(np.float).describe())


            # Create a big plot for the numerical columns
            subs = len(self.num_cols)
            cols = max(int(np.floor(np.sqrt(subs))),3)
            rows = min(cols + 1,2)
            if subs == 3:
                rows = 2
                cols = 2

            if subs == 8:

                rows = 3
                cols = 3

            if subs >= 20:

                rows = 6
                cols = 5

            print(cols)
            print(rows)
            print(len(self.num_cols))


            for j in range(2):

                df_fake_temp = df_fake.copy()
                df_real_temp = df_real.copy()
                df_fake_temp = df_fake_temp.loc[df_fake_temp[label_col] == labels[j], :]
                df_real_temp = df_real_temp.loc[df_real_temp[label_col] == labels[j], :]
                fig, axs = plt.subplots(nrows=rows, ncols=cols, figsize=(rows * 3, cols * 3))

                for i,c in enumerate(self.num_cols):

                    x = df_real_temp.loc[:,c].values
                    y = df_fake_temp.loc[:,c].values
                    xweights = 100 * np.ones_like(x) / x.size
                    yweights = 100 * np.ones_like(y) / y.size
                    axs[i // cols, i % cols].hist(x, weights=xweights, color='blue', alpha=0.5,bins=50)
                    axs[i // cols, i % cols].hist(y, weights=yweights, color='red', alpha=0.5,bins=50)
                    axs[i // cols, i % cols].set(title='{}'.format(c), ylabel='% of Dataset in Bin')
                    axs[i // cols, i % cols].margins(0.05)
                    axs[i // cols, i % cols].set_ylim(bottom=0)
                axs[0, 0].legend(["real", "fake"])
                plots_dir = os.path.join(root_path,"plots",evaluation_name)

                if save_results:
                    try:
                        os.makedirs(plots_dir)

                    except:
                        pass

                    fname_fig = os.path.join(plots_dir, "{}.png".format(names[j]))
                    plt.savefig(fname_fig, bbox_inches='tight')
                else:
                    print("results not saved")
                    # plt.show()

            df_real_corrs = df_real.copy()
            df_fake_corrs = df_fake.copy()
            if label_col == 'isFraud':
                df_real_corrs = df_real_corrs.drop('isFlaggedFraud',axis = 1)
                df_fake_corrs = df_fake_corrs.drop('isFlaggedFraud',axis = 1)

            df_real_fraud_corrs = df_real_corrs.loc[df_real_corrs[label_col] == labels[1], :]
            df_real_normal_corrs = df_real_corrs.loc[df_real_corrs[label_col] == labels[0], :]


            df_fake_fraud_corrs = df_fake_corrs.loc[df_fake_corrs[label_col] == labels[1], :]
            df_fake_normal_corrs = df_fake_corrs.loc[df_fake_corrs[label_col] == labels[0], :]

            real_normal_corrs = df_real_normal_corrs.corr()
            real_fraud_corrs = df_real_fraud_corrs.corr()

            fake_normal_corrs = df_fake_normal_corrs.corr()
            fake_fraud_corrs = df_fake_fraud_corrs.corr()

            diff_normal_corrs = real_normal_corrs.values-fake_normal_corrs.values
            diff_fraud_corrs = real_fraud_corrs.values-fake_fraud_corrs.values


            f1, ax = plt.subplots(figsize=(11, 9))
            sns.heatmap(fake_normal_corrs, xticklabels=fake_normal_corrs.columns, yticklabels=fake_normal_corrs.columns, annot=True)

            f2, ax = plt.subplots(figsize=(11, 9))
            sns.heatmap(real_normal_corrs, xticklabels=real_normal_corrs.columns, yticklabels=real_normal_corrs.columns, annot=True)

            f3, ax = plt.subplots(figsize=(11, 9))
            sns.heatmap(diff_normal_corrs, xticklabels=fake_normal_corrs.columns, yticklabels=fake_normal_corrs.columns, annot=True)

            f4, ax = plt.subplots(figsize=(11, 9))
            sns.heatmap(fake_fraud_corrs, xticklabels=fake_fraud_corrs.columns, yticklabels=fake_fraud_corrs.columns, annot=True)

            f5, ax = plt.subplots(figsize=(11, 9))
            sns.heatmap(real_fraud_corrs, xticklabels=real_fraud_corrs.columns, yticklabels=real_fraud_corrs.columns, annot=True)

            f6, ax = plt.subplots(figsize=(11, 9))
            sns.heatmap(diff_fraud_corrs, xticklabels=fake_fraud_corrs.columns, yticklabels=fake_fraud_corrs.columns, annot=True)


            if save_results:
                f1.savefig(os.path.join(plots_dir,"fake_normal_corrs.png"), bbox_inches = 'tight')
                f2.savefig(os.path.join(plots_dir,"real_normal_corrs.png"), bbox_inches='tight')
                f3.savefig(os.path.join(plots_dir,"diff_normal_corrs.png"), bbox_inches='tight')
                f4.savefig(os.path.join(plots_dir,"fake_fraud_corrs.png"), bbox_inches='tight')
                f5.savefig(os.path.join(plots_dir,"real_fraud_corrs.png"), bbox_inches='tight')
                f6.savefig(os.path.join(plots_dir,"diff_fraud_corrs.png"), bbox_inches='tight')


            else:

                print("results not saved")
                # f2.show()
                # f3.show()

        else:

            with open(os.path.join(root_path, "textfiles", "{}.txt".format(evaluation_name)), 'w') as outfile:

                for c in self.use_cols:

                    if self.cols_dict[c] == 'cat':

                        if save_results:

                            summary_real = df_real[c].astype(str).value_counts(normalize=True).head(15)
                            summary_fake = df_fake[c].astype(str).value_counts(normalize=True).head(15)


                            a = df_real[c].astype(str).value_counts(normalize = False)
                            b = df_fake[c].astype(str).value_counts(normalize= False)
                            nums = min(a.shape[0], b.shape[0], 5)
                            a = a.head(nums)
                            b = b.head(nums)

                            print(a)
                            print(b)

                            chisqs,p = chisq(a,b)


                            # Print to the text file
                            outfile.write(
                                "\nThe p-value for the chisquared test is {}\n".format(p))
                            # Print to the text file

                            outfile.write(
                                "\n Chi-sq p-value is {} \n".format(p))
                            outfile.write(
                                "\nSummary of real data for feature {} for label \n".format(c))
                            summary_real.to_string(outfile)
                            outfile.write(
                                "\nSummary of fake data for feature {} for label \n".format(c))
                            summary_fake.to_string(outfile)

                        else:

                            print(df_real[c].astype(str).value_counts(normalize=True).head(15))
                            print(df_fake[c].astype(str).value_counts(normalize=True).head(15))

                    else:
                        if save_results:
                            summary_real = df_real[c].astype(np.float).describe()
                            summary_fake = df_fake[c].astype(np.float).describe()

                            T, p = stats.mannwhitneyu(np.sort(df_real[c].values), np.sort(df_fake[c].values))
                            jsd = js(df_real[c].values, df_fake[c].values)


                            outfile.write("\nThe js divergence between the two is [%.6f] \n" % jsd)
                            outfile.write(
                                "\nT count and p-value of Mann-Whitney U test for feature {} are: [%.0f] [%.6f] \n".format(
                                    c) % (T, p))
                            outfile.write("\nSummary of real data for feature {} \n".format(c))
                            summary_real.to_string(outfile)
                            outfile.write("\nSummary of fake data for feature {} \n".format(c))
                            summary_fake.to_string(outfile)

                        else:
                            print(df_real[c].astype(np.float).describe())
                            print(df_fake[c].astype(np.float).describe())

        # Create a big plot for the numerical columns
        subs = len(self.num_cols)
        cols = max(int(np.floor(np.sqrt(subs))), 3)
        rows = min(cols + 1, 2)
        if subs == 3:
            rows = 2
            cols = 2

        if subs == 8:
            rows = 3
            cols = 3

        if subs >=20:

            rows = 6
            cols = 5

        print(cols)
        print(rows)
        print(len(self.num_cols))

        df_fake_temp = df_fake.copy()
        df_real_temp = df_real.copy()

        fig, axs = plt.subplots(nrows=rows, ncols=cols, figsize=(rows * 3, cols * 3))
        for i, c in enumerate(self.num_cols):
            x = df_real_temp.loc[:, c].values
            y = df_fake_temp.loc[:, c].values
            xweights = 100 * np.ones_like(x) / x.size
            yweights = 100 * np.ones_like(y) / y.size
            axs[i // cols, i % cols].hist(x, weights=xweights, color='blue', alpha=0.5,bins=50)
            axs[i // cols, i % cols].hist(y, weights=yweights, color='red', alpha=0.5,bins=50)
            axs[i // cols, i % cols].set(title='{}'.format(c), ylabel='% of Dataset in Bin')
            axs[i // cols, i % cols].margins(0.05)
            axs[i // cols, i % cols].set_ylim(bottom=0)
        axs[0, 0].legend(["real", "fake"])
        plots_dir = os.path.join(root_path, "plots", evaluation_name)

        if save_results:
            try:
                os.makedirs(plots_dir)

            except:
                pass

            fname_fig = os.path.join(plots_dir, "Univariates.png")
            plt.savefig(fname_fig, bbox_inches='tight')
        else:
            print("results not saved")
            # plt.show()

        df_real_corrs = df_real.copy()
        df_fake_corrs = df_fake.copy()
        fake_corrs = df_fake_corrs.corr()
        real_corrs = df_real_corrs.corr()
        diff_corrs = real_corrs.values - fake_corrs.values



        f1, ax = plt.subplots(figsize=(11, 9))
        sns.heatmap(fake_corrs, xticklabels=fake_corrs.columns, yticklabels=fake_corrs.columns,
                    annot=True)

        f2, ax = plt.subplots(figsize=(11, 9))
        sns.heatmap(real_corrs, xticklabels=real_corrs.columns, yticklabels=real_corrs.columns,
                    annot=True)

        f3, ax = plt.subplots(figsize=(11, 9))
        sns.heatmap(diff_corrs, xticklabels=real_corrs.columns, yticklabels=real_corrs.columns,
                    annot=True)

        if save_results:
            f1.savefig(os.path.join(plots_dir, "fake_corrs.png"), bbox_inches='tight')
            f2.savefig(os.path.join(plots_dir, "real__corrs.png"), bbox_inches='tight')
            f3.savefig(os.path.join(plots_dir, "diff_corrs.png"), bbox_inches='tight')



        else:

            print("results not saved")
            # f2.show()
            # f3.show()

        # return lambdas_real, lambdas_fake,V_real,V_fake

    def feature_space_tests(self, model_path, df_fake_encoded, df_train_encoded, evaluation_name):


        if "Cond" in evaluation_name:
            # First we have to get the test data through the discriminator
            x = tf.placeholder(tf.float32, [n_points, enc_dims],
                               name='discriminator_train_to_encode')  # Placeholder for real data
            labels = tf.placeholder(tf.float32, [None, 1],
                                         name='labels')  # Placeholder for the labels of the data that has been input
            x_sampler = LabelledData(file=real_data_path_encode, cat_cols=self.cat_cols,
                             use_cols=self.use_cols,label_column = 'isFraud')

        else:
            # First we have to get the test data through the discriminator
            x = tf.placeholder(tf.float32, [n_points, enc_dims],
                               name='discriminator_train_to_encode')  # Placeholder for real data

            x_sampler = Data(file=real_data_path_encode, cat_cols=self.cat_cols,
                             use_cols=self.use_cols)


        catEmbeddingDims = [min(max(int(round(disc_hyper[0] * np.log(v_size))), 2), v_size) for v_size in
                            x_sampler.cenc.cat_card]

        discDims = parseFlags_tuple(disc_hyper[1])

        if "Cond" in evaluation_name:

            disc =  ConditionalDiscriminator(dims=discDims, out_dims=disc_hyper[2],
                               dims_nCross=disc_hyper[3],
                               catEmbeddingDims=catEmbeddingDims, ohFeatureIndices=x_sampler.cenc.feature_indices,
                               numColsLen=len(x_sampler.cenc.num_cols))
            _, discriminator_output = disc(x,reuse=tf.AUTO_REUSE,labels = labels)


        else:
            disc =  discriminator(dims=discDims, out_dims=disc_hyper[2],
                               dims_nCross=disc_hyper[3],
                               catEmbeddingDims=catEmbeddingDims, ohFeatureIndices=x_sampler.cenc.feature_indices,
                               numColsLen=len(x_sampler.cenc.num_cols))


            _, discriminator_output = disc(x,reuse=tf.AUTO_REUSE)

        saver = tf.train.Saver()

        with tf.Session() as sess:
            saver.restore(sess, model_path)

            if "Cond" in evaluation_name:
                x_to_encode,labels_to_encode = x_sampler(n_points)
                real_d_out = sess.run(discriminator_output, feed_dict={x: x_to_encode,labels:labels_to_encode})

            else:
                x_to_encode = x_sampler(n_points)
                real_d_out = sess.run(discriminator_output, feed_dict={x: x_to_encode})


        df_test_encoded = pd.DataFrame(real_d_out)


        test_encoded = df_test_encoded.values
        train_encoded = df_train_encoded.values
        fake_encoded = df_fake_encoded.values

        nbrs = NearestNeighbors(n_neighbors=1, algorithm='ball_tree').fit(fake_encoded)
        distances_train, _ = nbrs.kneighbors(train_encoded)
        distances_test, _ = nbrs.kneighbors(test_encoded)

        print(distances_test.shape)
        print(distances_train.shape)
        test_stat,p_value = stats.ks_2samp(distances_train.reshape(distances_train.shape[0],),distances_test.reshape(distances_test.shape[0],))
        print("the test_stat for the KS test for the nearest neighbours is [%5.9F]"% test_stat)
        print("the p_value for the KS test for the nearest neighbours is [%1.9F]"% p_value)
        print("The numbers of nearest neighbours are {} and {}".format(distances_train.shape, distances_test.shape))


        # Create and save a plot fo the losses to asses the training stability
        training_plot_fname = os.path.join(root_path,"plots", evaluation_name,"Nearest Neighbours")

        x = np.log10(distances_train + 1)
        y = np.log10(distances_test + 1)
        xweights = 100 * np.ones_like(x) / x.size
        yweights = 100 * np.ones_like(y) / y.size
        fig, ax = plt.subplots()
        ax.hist(x, weights=xweights, color='blue', alpha=0.5,bins = 200 )
        ax.hist(y, weights=yweights, color='red', alpha=0.5, bins = 200)
        ax.set(title='Histogram Comparison', ylabel='% of Dataset in Bin')
        ax.margins(0.05)
        ax.set_ylim(bottom=0)
        ax.legend(["train", "test"])
        plt.title("Nearest neighbours in each set")
        plt.savefig(training_plot_fname, bbox_inches='tight')

    def classifier_filter(self,df_real, df_final, col_target, cols_toOneHot, cols_featuresNum, classifier,
                          size_test=0.3):

        df_final = df_final
        df_model = pd.concat((df_real, df_final), axis=0, ignore_index=True)
        data_full, target_full, numEnc, le_list_OH, oneHotEnc, colsOH, le_target = preprocessData_model(df_model,
                                                                                                        cols_featuresNum,
                                                                                                        cols_toOneHot,
                                                                                                        col_target)
        data_train, data_test, target_train, target_test, index_train, index_predict = train_test_split(data_full,
                                                                                                        target_full,
                                                                                                        df_model.index,
                                                                                                        test_size=size_test,
                                                                                                       random_state=0)



        if classifier == 'rf':
            clf = RandomForestClassifier(random_state=0,n_estimators=300)
            clf.fit(data_train, target_train)
        elif classifier == 'tree':
            clf = DecisionTreeClassifier(max_depth=10, max_features='auto', random_state=0)  # ,min_samples_leaf=10)
            clf.fit(data_train, target_train)
        elif classifier == 'logreg':
            clf = LogisticRegression(random_state=0)
            clf.fit(data_train, target_train)
        elif classifier == 'xgb':
            clf = XGBClassifier(max_depth=3, n_jobs=4)
            clf.fit(data_train, target_train)
        else:
            clf = KNeighborsClassifier(n_neighbors=int((data_train.shape[0]) ** 0.5), n_jobs=6)
            clf.fit(data_train, target_train)

        target_pred = clf.predict(data_test)
        print(classification_report(target_test, target_pred))
        print(confusion_matrix(target_test, target_pred))
        print(auroc(target_test,target_pred))


    def t_SNE_vis(self,df_real,df_fake,dims,evaluation_name=evaluation_name):

        cols_dict = self.cols_dict
        xcols = [col for col in df_real.columns if cols_dict[col] == 'num']

        df_real['label'] = np.zeros(df_real.shape[0])
        df_fake['label'] = np.ones(df_real.shape[0])

        df = pd.concat([df_real,df_fake],axis=0)
        df_subset = df.sample(2000)
        data_subset = df_subset[xcols].values

        time_start = time.time()
        if dims == 3:
            training_plot_fname = os.path.join(root_path, "plots", evaluation_name, "t-SNE")

            tsne = skman.TSNE(n_components=3, verbose=1, perplexity=40, n_iter=300)
            tsne_results = tsne.fit_transform(data_subset)
            print('t-SNE done! Time elapsed: {} seconds'.format(time.time() - time_start))

            df_subset['tsne-3d-one'] = tsne_results[:, 0]
            df_subset['tsne-3d-two'] = tsne_results[:, 1]
            df_subset['tsne-3d-three'] = tsne_results[:, 2]

            df_fake = df_subset.copy()
            df_fake = df_fake.loc[df_fake['label'] == 1, :]

            df_real = df_subset.copy()
            df_real = df_real.loc[df_real['label'] == 0, :]

            fig = plt.figure(figsize=(12, 7))
            ax = Axes3D(fig)
            ax.scatter(df_fake['tsne-3d-one'], df_fake['tsne-3d-two'], df_fake['tsne-3d-three'],
                       c='red', label='fake', s=10)
            ax.scatter(df_real['tsne-3d-one'], df_real['tsne-3d-two'], df_real['tsne-3d-three'],
                       c='blue', label='real', s=10)


            ax.legend()
            plt.savefig(training_plot_fname, bbox_inches='tight')



        else:
            tsne = skman.TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
            tsne_results = tsne.fit_transform(data_subset)
            print('t-SNE done! Time elapsed: {} seconds'.format(time.time() - time_start))
            df_subset['tsne-3d-one'] = tsne_results[:, 0]
            df_subset['tsne-3d-two'] = tsne_results[:, 1]

            df_fake = df_subset.copy()
            df_fake = df_fake.loc[df_fake['label'] == 1, :]

            df_real = df_subset.copy()
            df_real = df_real.loc[df_real['label'] == 0, :]

            fig, ax = plt.subplots(figsize=(11, 9))

            ax.scatter(df_fake['tsne-3d-one'], df_fake['tsne-3d-two'],
                       c='red', label='fake', s=5, alpha=0.5)
            ax.scatter(df_real['tsne-3d-one'], df_real['tsne-3d-two'],
                       c='blue', label='real', s=5, alpha=0.5)

            ax.legend()
            plt.show()





# Main function that we are going to be using for this evaluation
def main():

    # Define the column dictionaries
    if use_case == "PNR":

        cols_dict = {'Origin_Country': 'cat',
                      'Destination_Country': 'cat',
                      'OfficeIdCountry': 'cat',
                      'SaturdayStay': 'cat',
                      'PurchaseAnticipation': 'num',
                      'NumberPassenger': 'cat',
                      'StayDuration': 'num',
                      'Title': 'cat',
                      'TravelingWithChild': 'cat',
                      'ageAtPnrCreation': 'num',
                      'Nationality2': 'cat',
                      'BL': 'cat'}

        cols = ['Origin_Country','Destination_Country','OfficeIdCountry','SaturdayStay','PurchaseAnticipation','NumberPassenger',
                      'StayDuration','Title','TravelingWithChild','ageAtPnrCreation','Nationality2','BL']


    elif use_case in ["PAYSIM", "PAYSIM_balanced" , "PAYSIM_raw",
                      "PAYSIM_fraud","PAYSIM_GAN","PAYSIM_step","PAYSIM_SMOTE_raw",
                      "PAYSIM_raw_balanced",'PAYSIM_raw_fraud',"PAYSIM_raw_normal","PAYSIM_clean_normal"]:

        cols_dict = {'step': 'num',
                      'type': 'cat',
                      'amount': 'num',
                      'oldbalanceOrg': 'num',
                      'newbalanceOrig': 'num',
                      'oldbalanceDest': 'num',
                      'newbalanceDest': 'num',
                      'isFraud': 'cat',
                      'isFlaggedFraud': 'cat'}

        cols = ['step', 'type', 'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest',
                'isFraud',
                'isFlaggedFraud']


    elif use_case == 'pca_credit':

        cols_dict = {'Time': 'num', 'V1': 'num', 'V2': 'num', 'V3': 'num', 'V4': 'num', 'V5': 'num', 'V6': 'num', 'V7': 'num',
         'V8': 'num', 'V9': 'num', 'V10': 'num', 'V11': 'num', 'V12': 'num', 'V13': 'num', 'V14': 'num', 'V15': 'num',
         'V16': 'num', 'V17': 'num', 'V18': 'num', 'V19': 'num', 'V20': 'num', 'V21': 'num', 'V22': 'num', 'V23': 'num',
         'V24': 'num', 'V25': 'num', 'V26': 'num', 'V27': 'num', 'V28': 'num', 'Amount': 'num', 'Class': 'cat'}

        cols = ['Time', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'V7', 'V8', 'V9', 'V10',
           'V11', 'V12', 'V13', 'V14', 'V15', 'V16', 'V17', 'V18', 'V19', 'V20',
           'V21', 'V22', 'V23', 'V24', 'V25', 'V26', 'V27', 'V28', 'Amount',
           'Class']


    else:

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat',
                     'errorBalanceOrig':'num',
                     'errorBalanceDest': 'num'}

    classifier = 'rf'
    # Define the evaluator object
    eval = evaluator(cols_dict)



    # Get the data points for the correct data path
    df_real = pd.read_csv(real_data_path)
    df_fake = pd.read_csv(fake_data_path)
    df_real = df_real[cols]
    df_fake = df_fake[cols]
    n_subS = min(df_real.shape[0],df_fake.shape[0])
    df_fake = df_fake.sample(n=n_subS,random_state =0)
    df_real = df_real.sample(n=n_subS,random_state =0)
    print("The shape of the real data is {}".format(df_real.shape))
    print("The shape of the fake data is {}".format(df_fake.shape))



    # Get the data points for the feature space comparisons

    df_real_encode = pd.read_csv(real_data_path_encode)
    df_train_encoded = pd.read_csv(train_encoded_path)
    df_fake_encoded = pd.read_csv(fake_encoded_path)

    eval.column_tests(df_real=df_real, df_fake=df_fake, evaluation_name=evaluation_name)
    print("One")
    # eval.feature_space_tests(model_path=model_path,df_train_encoded = df_train_encoded,df_fake_encoded= df_fake_encoded,evaluation_name=evaluation_name)
    print("Two")
    eval.t_SNE_vis(df_real = df_real,df_fake=df_fake,dims=3,evaluation_name=evaluation_name)



    # Do a whole lot of stuff for the preprocessing for the classifier

    n_subS = min(df_real.shape[0],df_fake.shape[0],10000)
    df_fake = df_fake.sample(n=n_subS,random_state =0)
    df_real = df_real.sample(n=n_subS,random_state =0)

    # Multivariate evaluation: RF calssifier to separate real/synthetic data
    df_real_aux = df_real.head(df_fake.shape[0]).copy(deep=True)
    df_fake_aux = df_fake.copy(deep=True)

    df_fake_aux = df_fake_aux.sample(n=n_subS,random_state =0)
    df_real_aux = df_real_aux.sample(n=n_subS,random_state =0)

    col_target = 'label'
    cols_toOneHot = [c for c in cols_dict if cols_dict[c] == 'cat']
    cols_featuresNum = [c for c in cols_dict if cols_dict[c] == 'num']

    df_real_aux[cols_featuresNum] = df_real_aux[cols_featuresNum].astype(np.int)
    df_fake_aux[cols_featuresNum] = df_fake_aux[cols_featuresNum].astype(np.int)
    df_real_aux[cols_toOneHot] = df_real_aux[cols_toOneHot].astype(np.str)
    df_fake_aux[cols_toOneHot] = df_fake_aux[cols_toOneHot].astype(np.str)

    df_real_aux[col_target] = 1
    df_fake_aux[col_target] = 0

    eval.classifier_filter(df_real_aux, df_fake_aux, col_target, cols_toOneHot,
                          cols_featuresNum, classifier,size_test=0.3)


if __name__ == "__main__":

    main()

