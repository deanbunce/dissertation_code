# File to just see if we can load our saved model

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

MLP_VARIABLE_SCOPE = "MLP"

x = tf.placeholder(tf.float32, [None, 25])

x_data = np.array(np.arange(1000)).reshape(40,25)/1000

def predictor(data):

    with tf.variable_scope(MLP_VARIABLE_SCOPE, reuse=tf.AUTO_REUSE):
        print(data.shape)
        h = tf.layers.dense(data,32, activation=tf.nn.relu)
        print(h.shape)
        preds = tf.layers.dense(h,1,activation = tf.nn.relu)

    return preds

preds = predictor(x)
saver = tf.train.Saver()


with tf.Session() as sess:
    # Restore variables from disk.
    saver.restore(sess, "../models/MLP_TEST.ckpt")
    print("Model restored.")

    # So now we wat to get the word embeddings out of this tf restored thing and into a text file with their corresponding word
    returned_embeddings = sess.run(preds,feed_dict={x:x_data})
    print(returned_embeddings)
    plt.plot(returned_embeddings)
    plt.show()

