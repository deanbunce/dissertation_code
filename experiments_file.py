### Looking for why this doesn't work
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
fake_data = pd.read_csv("../generated_data/CondCramer_C_PAYSIM_SMOTE_step_epochs_45_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_10:45:52,10M2019-07-10_genM_0/fakeDecoded_iter50000.csv.gz")
test_data = pd.read_csv("../data/Paysim_SMOTE_step/test.csv")
train_data =pd.read_csv("../data/Paysim/train.csv")

fake_data = fake_data.loc[fake_data.isFraud == 1,:]
test_data = test_data.loc[test_data.isFraud == 1,:].sample(2000)
train_data = train_data.loc[train_data.isFraud == 1,:].sample(5000)


print(fake_data.shape)
dfs = [fake_data,test_data,train_data]
for df in dfs:
    df['errorBalanceOrig'] = df.newbalanceOrig + df.amount - df.oldbalanceOrg
    df['errorBalanceDest'] = df.oldbalanceDest + df.amount - df.newbalanceDest


# cols_list = ['errorBalanceOrig','errorBalanceDest','newbalanceOrig']
# cols = 2
# fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(10, 10))
#
# for i,c in enumerate(cols_list):
#
#     print("Printing the summaries for the fake data for feature {}".format(c))
#     print(fake_data[c].astype(np.float).describe())
#     print("Printing the summaries for the real data for feature {}".format(c))
#     print(real_data[c].astype(np.float).describe())
#
#
#     x = fake_data.loc[:,c].values
#     y = real_data.loc[:,c].values
#     xweights = 100 * np.ones_like(x) / x.size
#     yweights = 100 * np.ones_like(y) / y.size
#     axs[i].hist(x, weights=xweights, color='blue', alpha=0.5,bins = 50)
#     axs[i].hist(y, weights=yweights, color='red', alpha=0.5,bins=50)
#     axs[i].set(title='{}'.format(c), ylabel='% of Dataset in Bin')
#     axs[i].margins(0.05)
#     axs[i].set_ylim(bottom=0)
#
#
#
# axs[0].legend(["fake", "real"])
# fig.suptitle('Error balances (new balance + amount - old balance) for fraudulent data')

# things = ['oldbalanceOrg','newbalanceOrig','amount', 'oldbalanceDest','newbalanceDest',
#           'errorBalanceOrig','errorBalanceDest']
#
# train_data.plot.scatter('errorBalanceOrig','oldbalanceOrg',c='blue')
# fake_data.plot.scatter('errorBalanceOrig','oldbalanceOrg',c='red')
# test_data.plot.scatter('errorBalanceOrig','oldbalanceOrg',c='magenta')

# sns.pairplot(real_data[things])
# sns.pairplot(fake_data[things])


print(fake_data.step)
print(test_data.step)
plt.show()



