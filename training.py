# File for the creation of the different GANs and their trainingimport os
import os
import tensorflow as tf
import numpy as np
import pandas as pd
from dataManagement import Data, LabelledData
from networks import generator, discriminator, ConditionalDiscriminator,ConditionalGenerator
import logging_setup
import matplotlib.pyplot as plt
from fraudDetection import main_run
from datetime import date
import time
import datetime
now = datetime.datetime.now()

os.environ['CUDA_VISIBLE_DEVICES'] = "1"
seed = 42
np.random.seed(seed)
tf.set_random_seed(seed)

flags = tf.flags
##### COLAB ###########
flags.DEFINE_bool("colab",False,"run locally or on colab")

if flags.FLAGS.colab:
    flags.DEFINE_string("root_path", '/content/drive/My Drive/MSc_repo/', "Directory to save files.")
else:
    flags.DEFINE_string("root_path", '../', "Directory to save files.")

flags.DEFINE_string("model",default="WGAN",help = "Which model we want to train")
flags.DEFINE_string("type",default="",help = "Which type of conditioning to use")
flags.DEFINE_string("data_set",default = "PAYSIM_raw", help="Which data set we will train on")
flags.DEFINE_bool("save_model",False,"Save the model")
flags.DEFINE_bool("restore_model",False,"Using a saved vs a restored model")
flags.DEFINE_string("restore_from_path","","path from which to restore models")
tf.flags.DEFINE_integer("evaluate_every", 100, "evaluate every x batches")
tf.flags.DEFINE_integer("interv",1000000000, "data generation frequency in iterations")
flags.DEFINE_integer("max_epoch", 20, "Number of epochs before stopping training.")
flags.DEFINE_integer("batch_size", 256, "Batch size.") # default 256
flags.DEFINE_float("learning_rate", 0.0001,
                   "Learning rate") # Default 0.0001, 1e-4
flags.DEFINE_float("LAMBDA", 10.0,
                   "weight gradient penalty term in D loss")
flags.DEFINE_float("reg_scale", 0.00001,
                   "L2 regularization scale")
flags.DEFINE_boolean("disable_l2_regularizer", True,
                     "disable L2 regularization on weights")
flags.DEFINE_boolean("z_normal", False,
                     "use z from normal or uniform distribution") #Default is false
flags.DEFINE_integer("zDim", 10,
                     "dimension of z") # Default is 10
flags.DEFINE_boolean("use_dropout_everywhere", False,
                     "use droput between all layers")
flags.DEFINE_float("keep_prob", 0.5,
                   "Keep probability. 1.0 disables dropout")
flags.DEFINE_string("genDims_str", '(128,128)',
                    "size FC layers used in G") # Default (128,128)
flags.DEFINE_string("discDims_str", '(128,128)', # Default(128,128)
                    "size FC layers used in D")
flags.DEFINE_integer("discDims_nCross", 0,
                     "number cross-net layers")
flags.DEFINE_integer("discriminatorOut_dims", 1,
                     "discriminator output dims, the bigger the better for Cramer, 1 for WGAN") # Default is 64
flags.DEFINE_integer("n_critics", 5,
                     "number of iterations of D")
flags.DEFINE_integer("emb_vocab_min", 5,
                     "minimum token count to have its own embedding")
flags.DEFINE_integer("k", 5,
                     "embeddings dimension")
flags.DEFINE_integer("n_points", 12000,
                     "embeddings dimension") #default is 12000

flags.DEFINE_integer("gen_M", -1,
                     "index of last not shared layer in the generator")

FLAGS = flags.FLAGS

tensorboard_name = FLAGS.model + '_' + FLAGS.type + '_'+\
                   FLAGS.data_set +\
                   '_epochs_'+str(FLAGS.max_epoch) + '_latentSize_'+str(FLAGS.zDim) +'_learning rate_' +\
                    str(FLAGS.learning_rate)+ '_genDims_' + FLAGS.genDims_str+  'discDims' + FLAGS.discDims_str+ 'crossdims' + str(FLAGS.discDims_nCross) +'_'+\
                   now.strftime("%H:%M:%S,%dM")+ str(date.today()) + '_genM_' + str(FLAGS.gen_M)

name_experiment = tensorboard_name
print(name_experiment)
logs_dir = os.path.join(FLAGS.root_path, 'logs/',name_experiment)

try:
    os.makedirs(logs_dir)

except:
    print("logs dir already made")

logger = logging_setup.setup_logger(name_experiment, logs_dir= logs_dir, also_stdout=True)
logger.info('Starting the logs for the following experiment :  ' + name_experiment)

if FLAGS.data_set in ['PAYSIM' ,'PAYSIM_features' , 'PAYSIM_balanced' , "PAYSIM_SMOTE", 'PAYSIM_fraud' , 'PAYSIM_GAN',
                      'PAYSIM_features_fraud','PAYSIM_features_GAN','PAYSIM_features_balanced',
                      'PAYSIM_SMOTE_step','PAYSIM_raw','PAYSIM_raw_balanced','PAYSIM_SMOTE_raw',
                      'PAYSIM_raw_fraud','PAYSIM_raw_normal','PAYSIM_sub_normal',
                      'PAYSIM_sub_raw_normal','PAYSIM_raw_clean']:

    label_column = 'isFraud'

elif FLAGS.data_set == 'PNR':

    label_column = 'BL'

elif FLAGS.data_set in ['pca_credit','pca_credit_SMOTE']:

    label_column = 'Class'

else:
    label_column = None

print("This label column is {}".format(label_column))
# convert string format flag into tuple
def parseFlags_tuple(tuple_str):
    s = tuple_str[1:-1]
    t = s.split(',')

    return [int(d) for d in t]


class NoiseSampler(object):
    def __init__(self, z_dim, batch_size, z_normal):
        self.z_dim = z_dim
        self.batch_size = batch_size
        self.z_normal = z_normal

    def __call__(self, *args):
        if len(args) == 0:
            size = self.batch_size
        elif len(args) == 1:
            size = args[0]
        else:
            raise Exception('Too many inputs')

        if self.z_normal:
            random_inputs = np.random.normal(loc=0.0, scale=1.0, size=[size, self.z_dim])
        else:
            random_inputs = np.random.uniform(low=0.0, high=1.0, size=[size, self.z_dim])

        return random_inputs


class cramerCritic(object):
    def __init__(self, h):
        self.h = h

    def __call__(self, x, x_):
        _, h_x = self.h(x)
        _, h_x_ = self.h(x_)

        return tf.norm(h_x - h_x_, axis=1) - tf.norm(h_x, axis=1)

class CondCramerCritic(object):

    def __init__(self, h):
        self.h = h

    def __call__(self, x, x_,labels):
        _, h_x = self.h(x,labels=labels)
        _, h_x_ = self.h(x_,labels=labels)

        return tf.norm(h_x - h_x_, axis=1) - tf.norm(h_x, axis=1)



class WCritic(object):

    def __init__(self, h):
        self.h = h

    def __call__(self, x, x_):
        _, h_x = self.h(x)
        _, h_x_ = self.h(x_)

        return h_x - h_x_

class condWCritic(object):

    def __init__(self, h):
        self.h = h

    def __call__(self, x, x_,labels):
        _, h_x = self.h(x,labels= labels)
        _, h_x_ = self.h(x_,labels = labels)

        return h_x - h_x_


class CramerGAN(object):

    def __init__(self, x_dim, z_dim, g_net, d_net, x_sampler, z_sampler, scale=10.0, learning_rate=FLAGS.learning_rate):
        self.d_net = d_net  # discriminator network
        self.g_net = g_net  # generator network
        self.critic = cramerCritic(d_net)  # critic or h function that is being used for the loss
        self.x_sampler = x_sampler  # Is a Data() object from encode/decode
        self.z_sampler = z_sampler  # Is the noise sampler functino from above
        self.x_dim = x_dim  # Dimensions of the data that we have observed
        self.z_dim = z_dim  # latent dimensions
        self.x = tf.placeholder(tf.float32, [None, self.x_dim], name='x')  # Placeholder for real data
        self.z1 = tf.placeholder(tf.float32, [None, self.z_dim], name='z1')  # Placeholder for first latent sample
        self.z2 = tf.placeholder(tf.float32, [None, self.z_dim], name='z2')  # placeholder for second latent sample

        # Note how the Gnet produces the first argument here and the Dnet does the second
        self.x1_, _ = self.g_net(self.z1, reuse=False)
        self.x2_, _ = self.g_net(self.z2)

        _, self.d_out = d_net(self.x, reuse=False)

        self.g_loss = tf.reduce_mean(self.critic(self.x, self.x2_) - self.critic(self.x1_, self.x2_))
        self.d_loss = -self.g_loss

        # interpolate real and generated samples
        epsilon = tf.random_uniform([], 0.0, 1.0)
        x_hat = epsilon * self.x + (1 - epsilon) * self.x1_
        d_hat = self.critic(x_hat, self.x2_)

        ddx = tf.gradients(d_hat, x_hat)[0]
        ddx = tf.norm(ddx, axis=1)
        ddx = tf.reduce_mean(tf.square(ddx - 1.0) * scale)

        self.d_loss = self.d_loss + ddx

        self.ddx = ddx
        self.d_adam, self.g_adam = None, None
        with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
            self.d_adam = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0, beta2=0.9) \
                .minimize(self.d_loss, var_list=self.d_net.vars)
            self.g_adam = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0, beta2=0.9) \
                .minimize(self.g_loss, var_list=self.g_net.vars)

        gpu_options = tf.GPUOptions(allow_growth=True)
        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))


    def saveModel(self, saver, filenameModel):
        saver.save(self.sess, filenameModel)

    def train(self, dirname, batch_size, num_batches, interv, encoded_size, decoded_size,models_dir):

        # Create lists to store the generator and discriminator losses
        gen_losses = []
        disc_losses = []

        saver = tf.train.Saver()
        if FLAGS.restore_model:
            saver.restore(self.sess, FLAGS.restore_from_path)
            logger.info("Successfully restored model from {}".format(FLAGS.restore_from_path))
        else:
            self.sess.run(tf.global_variables_initializer())
            logger.info("Initialized Global Variables")

        start_time = time.time()
        logger.info("Number of batches to train {}".format(num_batches))

        for t in range(0, num_batches):
            d_iters = FLAGS.n_critics
            for _ in range(0, d_iters):
                bx = self.x_sampler(batch_size)
                bz1 = self.z_sampler()
                bz2 = self.z_sampler()
                self.sess.run(self.d_adam, feed_dict={self.x: bx, self.z1: bz1, self.z2: bz2})

            bx = self.x_sampler(batch_size)
            bz1 = self.z_sampler()
            bz2 = self.z_sampler()
            self.sess.run(self.g_adam, feed_dict={self.z1: bz1, self.x: bx, self.z2: bz2})

            if t % FLAGS.evaluate_every == 0:

                bx = self.x_sampler(batch_size)
                bz1 = self.z_sampler()
                bz2 = self.z_sampler()
                d_loss,ddx_ = self.sess.run(
                    [self.d_loss,self.ddx], feed_dict={self.x: bx, self.z1: bz1, self.z2: bz2}
                )
                g_loss = self.sess.run(
                    self.g_loss, feed_dict={self.z1: bz1, self.z2: bz2, self.x: bx}
                )
                logger.info('Iter [%8d] Time [%5.4f] d_loss [%.4f] g_loss [%.4f] ddx [%.4f]' %
                      (t, time.time() - start_time, d_loss, g_loss, ddx_))

                gen_losses.append(g_loss)
                disc_losses.append(d_loss)

                if FLAGS.save_model:
                    fname_model = os.path.join(models_dir,'t_%d.ckpt' % t)
                    self.saveModel(saver, fname_model)
                    logger.info('Stored the model')

            if (t % interv == 0 and t != 0) or (t == num_batches-1):

                # Create the path names
                real_encodedfnameout = os.path.join(dirname,"realEncoded_iter{}.csv".format(t))
                fake_encodedfnameout = os.path.join(dirname , "fakeEncoded_iter{}.csv".format(t))

                fake_decodedfnameout = os.path.join(dirname, "fakeDecoded_iter{}.csv".format(t))
                real_decodedfnameout = os.path.join(dirname , "realDecoded_iter{}.csv".format(t))

                # Fake samples
                decodedSize = decoded_size
                bz1 = self.z_sampler(decodedSize)
                bx = self.sess.run(self.x1_, feed_dict={self.z1: bz1})

                # subsample encoded data to avoid large files
                subn = encoded_size
                full = np.arange(bx.shape[0])
                choice = np.random.choice(full, subn)
                bx_sub = bx[choice, :]

                fake_d_out = self.sess.run(self.d_out, feed_dict={self.x: bx_sub})

                # real samples
                realSubsample = self.x_sampler(decodedSize)
                # subsample encoded data to avoid large files
                full = np.arange(decodedSize)
                choice = np.random.choice(full, subn)
                realSubsample_sub = realSubsample[choice, :]

                real_d_out = self.sess.run(self.d_out, feed_dict={self.x: realSubsample_sub})

                np.savetxt(real_encodedfnameout + ".gz", real_d_out, delimiter=',', fmt='%.2e')
                np.savetxt(fake_encodedfnameout + ".gz", fake_d_out, delimiter=',', fmt='%.2e')

                # Decode and save the files
                dfdec = self.x_sampler.cenc.inverse_transform(realSubsample)
                dfout = self.x_sampler.cenc.inverse_transform(bx)

                dfdec.to_csv(real_decodedfnameout + ".gz", index=False, compression="gzip",
                             float_format='%.2f')
                dfout.to_csv(fake_decodedfnameout + ".gz", index=False, compression="gzip",
                             float_format='%.2f')

        return gen_losses, disc_losses

    def sampler(self, n_batches):

        batches_res = []
        for i in range(n_batches):
            bz1 = self.z_sampler()
            bx = self.sess.run(self.x1_, feed_dict={self.z1: bz1})
            batches_res.append(np.squeeze(bx))

        returnable = np.concatenate(batches_res, axis=0)
        dfout = self.x_sampler.cenc.inverse_transform(returnable)

        return dfout

    def apply_h(self, dirname, ckpt_fname, input_fname, original_cat_cols, use_cols):
        saver = tf.train.Saver()
        saver.restore(self.sess, ckpt_fname)

        encoded_output_fName = os.path.join(dirname, os.path.basename(input_fname)) + "_encodedH.csv"

        data_in = pd.read_csv(input_fname, usecols=use_cols)

        input_chunk = self.x_sampler.cenc.transform(data_in, original_cat_cols)
        input_d_out = self.sess.run(self.d_out, feed_dict={self.x: input_chunk})
        np.savetxt(encoded_output_fName, input_d_out, delimiter=',', fmt='%.2e')

class ConditionalCramerGAN(object):

    def __init__(self, x_dim, z_dim, g_net, d_net, x_sampler, z_sampler, scale=10.0, learning_rate=FLAGS.learning_rate):
        self.d_net = d_net  # discriminator network
        self.g_net = g_net  # generator network
        self.critic = CondCramerCritic(d_net)  # critic or h function that is being used for the loss
        self.x_sampler = x_sampler  # Is a Data() object from encode/decode
        self.z_sampler = z_sampler  # Is the noise sampler functino from above
        self.x_dim = x_dim  # Dimensions of the data that we have observed
        self.z_dim = z_dim  # latent dimensions
        self.x = tf.placeholder(tf.float32, [None, self.x_dim], name='x')  # Placeholder for real data
        self.z1 = tf.placeholder(tf.float32, [None, self.z_dim], name='z1')  # Placeholder for first latent sample
        self.z2 = tf.placeholder(tf.float32, [None, self.z_dim], name='z2')  # placeholder for second latent sample
        self.labels = tf.placeholder(tf.float32,[None, 1], name = 'labels') # Placeholder for the labels of the data that has been input

        # Note how the Gnet produces the first argument here and the Dnet does the second
        self.x1_, _ = self.g_net(self.z1, reuse=False,labels=self.labels)
        self.x2_, _ = self.g_net(self.z2,labels = self.labels)

        _, self.d_out = d_net(self.x, reuse=False,labels=self.labels)

        self.g_loss = tf.reduce_mean(self.critic(self.x, self.x2_,labels=self.labels) - self.critic(self.x1_, self.x2_,labels=self.labels))
        self.d_loss = -self.g_loss

        # interpolate real and generated samples
        epsilon = tf.random_uniform([], 0.0, 1.0)
        x_hat = epsilon * self.x + (1 - epsilon) * self.x1_
        d_hat = self.critic(x_hat, self.x2_,labels=self.labels)

        ddx = tf.gradients(d_hat, x_hat)[0]
        ddx = tf.norm(ddx, axis=1)
        ddx = tf.reduce_mean(tf.square(ddx - 1.0) * scale)

        self.d_loss = self.d_loss + ddx

        self.ddx = ddx
        self.d_adam, self.g_adam = None, None
        with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
            self.d_adam = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0, beta2=0.9) \
                .minimize(self.d_loss, var_list=self.d_net.vars)
            self.g_adam = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0, beta2=0.9) \
                .minimize(self.g_loss, var_list=self.g_net.vars)

        gpu_options = tf.GPUOptions(allow_growth=True)
        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))


    def saveModel(self, saver, filenameModel):
        saver.save(self.sess, filenameModel)

    def train(self, dirname, batch_size, num_batches, interv, encoded_size, decoded_size,models_dir):

        # Create lists to store the generator and discriminator losses
        gen_losses = []
        disc_losses = []

        saver = tf.train.Saver()
        if FLAGS.restore_model:
            saver.restore(self.sess, FLAGS.restore_from_path)
            logger.info("Successfully restored model from {}".format(FLAGS.restore_from_path))
        else:
            self.sess.run(tf.global_variables_initializer())
            logger.info("Initialized Global Variables")

        start_time = time.time()
        logger.info("Number of batches to train {}".format(num_batches))

        for t in range(0, num_batches):
            d_iters = FLAGS.n_critics
            for _ in range(0, d_iters):
                bx, labels = self.x_sampler(batch_size)
                bz1 = self.z_sampler()
                bz2 = self.z_sampler()
                self.sess.run(self.d_adam, feed_dict={self.x: bx, self.z1: bz1, self.z2: bz2,self.labels:labels})

            bx,labels = self.x_sampler(batch_size)
            bz1 = self.z_sampler()
            bz2 = self.z_sampler()
            self.sess.run(self.g_adam, feed_dict={self.z1: bz1, self.x: bx, self.z2: bz2, self.labels: labels})

            if t % FLAGS.evaluate_every == 0:

                bx, labels = self.x_sampler(batch_size)
                bz1 = self.z_sampler()
                bz2 = self.z_sampler()
                d_loss,ddx_ = self.sess.run(
                    [self.d_loss,self.ddx], feed_dict={self.x: bx, self.z1: bz1, self.z2: bz2,self.labels:labels}
                )
                g_loss = self.sess.run(
                    self.g_loss, feed_dict={self.z1: bz1, self.z2: bz2, self.x: bx,self.labels: labels}
                )
                logger.info('Iter [%8d] Time [%5.4f] d_loss [%.4f] g_loss [%.4f] ddx [%.4f]' %
                      (t, time.time() - start_time, d_loss, g_loss,ddx_))

                gen_losses.append(g_loss)
                disc_losses.append(d_loss)

                if FLAGS.save_model:
                    fname_model = os.path.join(models_dir,'t_%d.ckpt' % t)
                    self.saveModel(saver, fname_model)
                    logger.info('Stored the model')

            if (t % interv == 0 and t != 0) or (t == num_batches-1):

                # Create the path names
                real_encodedfnameout = os.path.join(dirname,"realEncoded_iter{}.csv".format(t))
                fake_encodedfnameout = os.path.join(dirname , "fakeEncoded_iter{}.csv".format(t))

                fake_decodedfnameout = os.path.join(dirname, "fakeDecoded_iter{}.csv".format(t))
                real_decodedfnameout = os.path.join(dirname , "realDecoded_iter{}.csv".format(t))

                # Fake samples
                decodedSize = decoded_size
                generation_labels = np.random.binomial(1,p=self.x_sampler.p,size=decoded_size).reshape(decoded_size,1)
                bz1 = self.z_sampler(decodedSize)
                bx = self.sess.run(self.x1_, feed_dict={self.z1: bz1, self.labels: generation_labels})

                # subsample encoded data to avoid large files
                subn = encoded_size
                full = np.arange(bx.shape[0])
                choice = np.random.choice(full, subn)
                bx_sub = bx[choice, :]
                generation_labels_sub =  generation_labels[choice,:]

                fake_d_out = self.sess.run(self.d_out, feed_dict={self.x: bx_sub,self.labels:generation_labels_sub})

                # real samples
                realSubsample, realSubsample_labels = self.x_sampler(decodedSize)
                # subsample encoded data to avoid large files
                full = np.arange(decodedSize)
                choice = np.random.choice(full, subn)
                realSubsample_sub = realSubsample[choice, :]
                realSubsample_sub_labels = realSubsample_labels[choice, :]

                real_d_out = self.sess.run(self.d_out, feed_dict={self.x: realSubsample_sub,
                                                                  self.labels: realSubsample_sub_labels})

                np.savetxt(real_encodedfnameout + ".gz", real_d_out, delimiter=',', fmt='%.2e')
                np.savetxt(fake_encodedfnameout + ".gz", fake_d_out, delimiter=',', fmt='%.2e')

                # Decode and save the files
                dfdec = self.x_sampler.cenc.inverse_transform(realSubsample)
                dfout = self.x_sampler.cenc.inverse_transform(bx)

                dfdec.to_csv(real_decodedfnameout + ".gz", index=False, compression="gzip",
                             float_format='%.2f')
                dfout.to_csv(fake_decodedfnameout + ".gz", index=False, compression="gzip",
                             float_format='%.2f')

                which_fake_data = fake_decodedfnameout + ".gz"
                main_run(which_fake_data,colab= FLAGS.colab,raw=True,use_case="pca_credit")

        return gen_losses, disc_losses

    def sampler(self, n_batches):

        batches_res = []
        for i in range(n_batches):
            bz1 = self.z_sampler()
            generation_labels = np.random.binomial(1, p=self.x_sampler.p, size=bz1.shape[0]).reshape(bz1.shape[0],1)
            bx = self.sess.run(self.x1_, feed_dict={self.z1: bz1, self.labels: generation_labels})
            batches_res.append(np.squeeze(bx))

        returnable = np.concatenate(batches_res, axis=0)
        dfout = self.x_sampler.cenc.inverse_transform(returnable)

        return dfout

    def apply_h(self, dirname, ckpt_fname, input_fname, original_cat_cols, use_cols):
        saver = tf.train.Saver()
        saver.restore(self.sess, ckpt_fname)

        encoded_output_fName = os.path.join(dirname, os.path.basename(input_fname)) + "_encodedH.csv"

        data_in = pd.read_csv(input_fname, usecols=use_cols)

        input_chunk = self.x_sampler.cenc.transform(data_in, original_cat_cols)
        input_d_out = self.sess.run(self.d_out, feed_dict={self.x: input_chunk})
        np.savetxt(encoded_output_fName, input_d_out, delimiter=',', fmt='%.2e')





class WGAN(object):

    def __init__(self, x_dim, z_dim, g_net, d_net, x_sampler, z_sampler, scale=10.0, learning_rate=FLAGS.learning_rate):
        self.d_net = d_net  # discriminator network
        self.g_net = g_net  # generator network
        self.critic = WCritic(d_net)  # critic or h function that is being used for the loss
        self.x_sampler = x_sampler  # Is a Data() object from encode/decode
        self.z_sampler = z_sampler  # Is the noise sampler function from above
        self.x_dim = x_dim  # Dimensions of the data that we have observed
        self.z_dim = z_dim  # latent dimensions
        self.x = tf.placeholder(tf.float32, [None, self.x_dim], name='x')  # Placeholder for real data
        self.z1 = tf.placeholder(tf.float32, [None, self.z_dim], name='z1')  # Placeholder for first latent sample


        # Note how the Gnet produces the first argument here and the Dnet does the second
        self.x1_, _ = self.g_net(self.z1, reuse=False)
        _, self.d_out = d_net(self.x, reuse=False)
        self.d_loss = tf.reduce_mean(self.critic(self.x, self.x1_))
        self.g_loss = -self.d_loss

        # interpolate real and generated samples
        epsilon = tf.random_uniform([], 0.0, 1.0)
        x_hat = epsilon * self.x + (1 - epsilon) * self.x1_

        _,d_hat = d_net(x_hat)
        ddx = tf.gradients(d_hat, x_hat)[0]
        ddx = tf.norm(ddx, axis=1)

        ddx = tf.reduce_mean(tf.square(ddx - 1.0))*scale
        self.ddx = ddx
        self.d_loss = self.d_loss + self.ddx

        self.d_adam, self.g_adam = None, None
        with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
            self.d_adam = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0.5, beta2=0.9) \
                .minimize(self.d_loss, var_list=self.d_net.vars)
            self.g_adam = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0.5, beta2=0.9) \
                .minimize(self.g_loss, var_list=self.g_net.vars)

        gpu_options = tf.GPUOptions(allow_growth=True)

        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

    def saveModel(self, saver, filenameModel):
        saver.save(self.sess, filenameModel)

    def train(self, dirname, batch_size, num_batches, interv, encoded_size, decoded_size, models_dir):

        # Create lists to store the generator and discriminator losses
        gen_losses = []
        disc_losses = []

        saver = tf.train.Saver()
        if FLAGS.restore_model:
            saver.restore(self.sess, FLAGS.restore_from_path)
            logger.info("Successfully restored model from {}".format(FLAGS.restore_from_path))
        else:
            self.sess.run(tf.global_variables_initializer())
            logger.info("Initialized Global Variables")

        start_time = time.time()
        logger.info("Number of batches to train {}".format(num_batches))

        for t in range(0, num_batches):
            d_iters = FLAGS.n_critics
            for _ in range(0, d_iters):
                bx = self.x_sampler(batch_size)
                bz1 = self.z_sampler()
                self.sess.run(self.d_adam, feed_dict={self.x: bx, self.z1: bz1})

            bx = self.x_sampler(batch_size)
            bz1 = self.z_sampler()
            self.sess.run(self.g_adam, feed_dict={self.z1: bz1, self.x: bx})

            if t % FLAGS.evaluate_every == 0:

                bx = self.x_sampler(batch_size)
                bz1 = self.z_sampler()
                d_loss = self.sess.run(
                    self.d_loss, feed_dict={self.x: bx, self.z1: bz1}
                )
                g_loss = self.sess.run(
                    self.g_loss, feed_dict={self.z1: bz1, self.x: bx}
                )
                logger.info('Iter [%8d] Time [%5.4f] d_loss [%.4f] g_loss [%.4f]' %
                            (t, time.time() - start_time, d_loss, g_loss))

                gen_losses.append(g_loss)
                disc_losses.append(d_loss)

                if FLAGS.save_model:
                    fname_model = os.path.join(models_dir, 't_%d.ckpt' % t)
                    self.saveModel(saver, fname_model)
                    logger.info('Stored the model')

            if (t % interv == 0 and t != 0) or (t == num_batches - 1):
                # Create the path names
                real_encodedfnameout = os.path.join(dirname, "realEncoded_iter{}.csv".format(t))
                fake_encodedfnameout = os.path.join(dirname, "fakeEncoded_iter{}.csv".format(t))

                fake_decodedfnameout = os.path.join(dirname, "fakeDecoded_iter{}.csv".format(t))
                real_decodedfnameout = os.path.join(dirname, "realDecoded_iter{}.csv".format(t))

                # Fake samples
                decodedSize = decoded_size
                bz1 = self.z_sampler(decodedSize)
                bx = self.sess.run(self.x1_, feed_dict={self.z1: bz1})

                # subsample encoded data to avoid large files
                subn = encoded_size
                full = np.arange(bx.shape[0])
                choice = np.random.choice(full, subn)
                bx_sub = bx[choice, :]

                fake_d_out = self.sess.run(self.d_out, feed_dict={self.x: bx_sub})

                # real samples
                realSubsample = self.x_sampler(decodedSize)
                # subsample encoded data to avoid large files
                full = np.arange(decodedSize)
                choice = np.random.choice(full, subn)
                realSubsample_sub = realSubsample[choice, :]

                real_d_out = self.sess.run(self.d_out, feed_dict={self.x: realSubsample_sub})

                np.savetxt(real_encodedfnameout + ".gz", real_d_out, delimiter=',', fmt='%.2e')
                np.savetxt(fake_encodedfnameout + ".gz", fake_d_out, delimiter=',', fmt='%.2e')

                # Decode and save the files
                dfdec = self.x_sampler.cenc.inverse_transform(realSubsample)
                dfout = self.x_sampler.cenc.inverse_transform(bx)

                dfdec.to_csv(real_decodedfnameout + ".gz", index=False, compression="gzip",
                             float_format='%.2f')
                dfout.to_csv(fake_decodedfnameout + ".gz", index=False, compression="gzip",
                             float_format='%.2f')

                # save trained model
                filenameModel = os.path.join(models_dir, "model_at_point_{}.ckpt".format(t))
                saver = tf.train.Saver()
                self.saveModel(saver, filenameModel)
                print("Saving the model")

        return gen_losses, disc_losses

    def sampler(self, n_batches):

        batches_res = []
        for i in range(n_batches):
            bz1 = self.z_sampler()
            bx = self.sess.run(self.x1_, feed_dict={self.z1: bz1})
            batches_res.append(np.squeeze(bx))

        returnable = np.concatenate(batches_res, axis=0)
        dfout = self.x_sampler.cenc.inverse_transform(returnable)

        return dfout

class ConditionalWGAN(object):

    def __init__(self, x_dim, z_dim, g_net, d_net, x_sampler, z_sampler, scale=10.0, learning_rate=FLAGS.learning_rate):
        self.d_net = d_net  # discriminator network
        self.g_net = g_net  # generator network
        self.critic = condWCritic(d_net)  # critic or h function that is being used for the loss
        self.x_dim = x_dim  # Dimensions of the data that we have observed
        self.z_dim = z_dim  # latent dimensions
        self.x_sampler = x_sampler  # Is a Data() object from encode/decode
        self.z_sampler = z_sampler  # Is the noise sampler function from above
        self.x = tf.placeholder(tf.float32, [None, self.x_dim], name='x')  # Placeholder for real data
        self.z1 = tf.placeholder(tf.float32, [None, self.z_dim], name='z1')  # Placeholder for first latent sample
        self.labels = tf.placeholder(tf.float32,[None, 1], name = 'labels') # Placeholder for the labels of the data that has been input


        self.x1_, _ = self.g_net(self.z1, reuse=False,labels = self.labels)
        _, self.d_out = d_net(self.x,labels = self.labels, reuse=False)
        self.d_loss = tf.reduce_mean(self.critic(self.x, self.x1_,labels=self.labels))
        self.g_loss = -self.d_loss

        # interpolate real and generated samples
        epsilon = tf.random_uniform([], 0.0, 1.0)
        x_hat = epsilon * self.x + (1 - epsilon) * self.x1_

        _,d_hat = d_net(x_hat,labels = self.labels)
        ddx = tf.gradients(d_hat, x_hat)[0]
        ddx = tf.norm(ddx, axis=1)

        ddx = tf.reduce_mean(tf.square(ddx - 1.0))*scale
        self.ddx = ddx
        self.d_loss = self.d_loss + self.ddx

        self.d_adam, self.g_adam = None, None
        with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
            self.d_adam = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0.5, beta2=0.9) \
                .minimize(self.d_loss, var_list=self.d_net.vars)
            self.g_adam = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0.5, beta2=0.9) \
                .minimize(self.g_loss, var_list=self.g_net.vars)

        gpu_options = tf.GPUOptions(allow_growth=True)

        self.sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))


    def saveModel(self, saver, filenameModel):
        saver.save(self.sess, filenameModel)

    def train(self, dirname, batch_size, num_batches, interv, encoded_size, decoded_size,models_dir):

        # Create lists to store the generator and discriminator losses
        gen_losses = []
        disc_losses = []

        saver = tf.train.Saver()
        if FLAGS.restore_model:
            saver.restore(self.sess, FLAGS.restore_from_path)
            logger.info("Successfully restored model from {}".format(FLAGS.restore_from_path))
        else:
            self.sess.run(tf.global_variables_initializer())
            logger.info("Initialized Global Variables")

        start_time = time.time()
        logger.info("Number of batches to train {}".format(num_batches))

        for t in range(0, num_batches):
            d_iters = FLAGS.n_critics
            for _ in range(0, d_iters):
                bx, labels = self.x_sampler(batch_size)
                bz1 = self.z_sampler()
                bz2 = self.z_sampler()
                self.sess.run(self.d_adam, feed_dict={self.x: bx, self.z1: bz1,self.labels:labels})

            bx,labels = self.x_sampler(batch_size)
            bz1 = self.z_sampler()
            bz2 = self.z_sampler()
            self.sess.run(self.g_adam, feed_dict={self.z1: bz1, self.x: bx, self.labels: labels})

            if t % FLAGS.evaluate_every == 0:

                bx, labels = self.x_sampler(batch_size)
                bz1 = self.z_sampler()
                d_loss,ddx_ = self.sess.run(
                    [self.d_loss,self.ddx], feed_dict={self.x: bx, self.z1: bz1,self.labels:labels}
                )
                g_loss = self.sess.run(
                    self.g_loss, feed_dict={self.z1: bz1, self.x: bx,self.labels: labels}
                )
                logger.info('Iter [%8d] Time [%5.4f] d_loss [%.4f] g_loss [%.4f] ddx [%.4f]' %
                      (t, time.time() - start_time, d_loss, g_loss,ddx_))

                gen_losses.append(g_loss)
                disc_losses.append(d_loss)

                if FLAGS.save_model:
                    fname_model = os.path.join(models_dir,'t_%d.ckpt' % t)
                    self.saveModel(saver, fname_model)
                    logger.info('Stored the model')

            if (t % interv == 0 and t != 0) or (t == num_batches-1):

                # Create the path names
                real_encodedfnameout = os.path.join(dirname,"realEncoded_iter{}.csv".format(t))
                fake_encodedfnameout = os.path.join(dirname , "fakeEncoded_iter{}.csv".format(t))

                fake_decodedfnameout = os.path.join(dirname, "fakeDecoded_iter{}.csv".format(t))
                real_decodedfnameout = os.path.join(dirname , "realDecoded_iter{}.csv".format(t))

                # Fake samples
                decodedSize = decoded_size
                generation_labels = np.random.binomial(1,p=self.x_sampler.p,size=decoded_size).reshape(decoded_size,1)
                bz1 = self.z_sampler(decodedSize)
                bx = self.sess.run(self.x1_, feed_dict={self.z1: bz1, self.labels: generation_labels})

                # subsample encoded data to avoid large files
                subn = encoded_size
                full = np.arange(bx.shape[0])
                choice = np.random.choice(full, subn)
                bx_sub = bx[choice, :]
                generation_labels_sub =  generation_labels[choice,:]

                fake_d_out = self.sess.run(self.d_out, feed_dict={self.x: bx_sub,self.labels:generation_labels_sub})

                # real samples
                realSubsample, realSubsample_labels = self.x_sampler(decodedSize)
                # subsample encoded data to avoid large files
                full = np.arange(decodedSize)
                choice = np.random.choice(full, subn)
                realSubsample_sub = realSubsample[choice, :]
                realSubsample_sub_labels = realSubsample_labels[choice, :]

                real_d_out = self.sess.run(self.d_out, feed_dict={self.x: realSubsample_sub,
                                                                  self.labels: realSubsample_sub_labels})

                np.savetxt(real_encodedfnameout + ".gz", real_d_out, delimiter=',', fmt='%.2e')
                np.savetxt(fake_encodedfnameout + ".gz", fake_d_out, delimiter=',', fmt='%.2e')

                # Decode and save the files
                dfdec = self.x_sampler.cenc.inverse_transform(realSubsample)
                dfout = self.x_sampler.cenc.inverse_transform(bx)

                dfdec.to_csv(real_decodedfnameout + ".gz", index=False, compression="gzip",
                             float_format='%.2f')
                dfout.to_csv(fake_decodedfnameout + ".gz", index=False, compression="gzip",
                             float_format='%.2f')

                which_fake_data = fake_decodedfnameout + ".gz"
                main_run(which_fake_data,colab= FLAGS.colab,raw=False,use_case="PNR")

        return gen_losses, disc_losses

    def sampler(self, n_batches):

        batches_res = []
        for i in range(n_batches):
            bz1 = self.z_sampler()
            generation_labels = np.random.binomial(1, p=self.x_sampler.p, size=bz1.shape[0]).reshape(bz1.shape[0],1)
            bx = self.sess.run(self.x1_, feed_dict={self.z1: bz1, self.labels: generation_labels})
            batches_res.append(np.squeeze(bx))

        returnable = np.concatenate(batches_res, axis=0)
        dfout = self.x_sampler.cenc.inverse_transform(returnable)

        return dfout

    def apply_h(self, dirname, ckpt_fname, input_fname, original_cat_cols, use_cols):
        saver = tf.train.Saver()
        saver.restore(self.sess, ckpt_fname)

        encoded_output_fName = os.path.join(dirname, os.path.basename(input_fname)) + "_encodedH.csv"

        data_in = pd.read_csv(input_fname, usecols=use_cols)

        input_chunk = self.x_sampler.cenc.transform(data_in, original_cat_cols)
        input_d_out = self.sess.run(self.d_out, feed_dict={self.x: input_chunk})
        np.savetxt(encoded_output_fName, input_d_out, delimiter=',', fmt='%.2e')




##################

def main(_):

    genDims = parseFlags_tuple(FLAGS.genDims_str)
    discDims = parseFlags_tuple(FLAGS.discDims_str)

    # summary and other directories these will be very useful later
    summaries_dir = os.path.join(FLAGS.root_path, 'summaries')
    plots_dir = os.path.join(FLAGS.root_path, 'plots')
    generated_data_dir = os.path.join(FLAGS.root_path, 'generated_data',name_experiment)
    models_dir = os.path.join(FLAGS.root_path,'models',name_experiment)

    try:
        os.makedirs(summaries_dir)
    except:
        pass
    try:
        os.makedirs(plots_dir)
    except:
        pass
    try:
        os.makedirs(generated_data_dir)
    except:
        pass
    try:
        os.makedirs(models_dir)
    except:
        pass

    if FLAGS.data_set == 'PNR':
        if FLAGS.colab:
            filenameIn = '/content/drive/My Drive/MSc_repo/data/processedAirplane/train.csv'


        else:
            filenameIn = '../data/processedAirplane/train.csv'
        cols_dict = {'Origin_Country': 'cat',
                     'Destination_Country': 'cat',
                     'OfficeIdCountry': 'cat',
                     'SaturdayStay': 'cat',
                     'PurchaseAnticipation': 'num',
                     'NumberPassenger': 'cat',
                     'StayDuration': 'num',
                     'Title': 'cat',
                     'TravelingWithChild': 'cat',
                     'ageAtPnrCreation': 'num',
                     'Nationality2': 'cat',
                     'BL': 'cat'}

    elif FLAGS.data_set == "PAYSIM":

        if FLAGS.colab:
            filenameIn = '/content/drive/My Drive/MSc_repo/data/Paysim/train.csv'

        else:
            filenameIn = '../data/Paysim/train.csv'
        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}

    elif FLAGS.data_set == "PAYSIM_balanced":

        if FLAGS.colab:
            filenameIn = '/content/drive/My Drive/MSc_repo/data/Paysim_balanced/train.csv'

        else:
            filenameIn = '../data/Paysim_balanced/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}


    elif FLAGS.data_set == "PAYSIM_raw":

        if FLAGS.colab:
            filenameIn = '/content/drive/My Drive/MSc_repo/data/Paysim_raw/train.csv'

        else:
            filenameIn = '../data/Paysim_raw/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}

    elif FLAGS.data_set == "PAYSIM_features":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/Paysim_features/train.csv'

        else:
            filenameIn = '../data/Paysim_features/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat',
                     'errorBalanceOrig':'num',
                     'errorBalanceDest': 'num'}

    elif FLAGS.data_set == "PAYSIM_features_GAN":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/Paysim_features_GAN/train.csv'

        else:
            filenameIn = '../data/Paysim_features_GAN/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat',
                     'errorBalanceOrig':'num',
                     'errorBalanceDest': 'num'}

    elif FLAGS.data_set == "PAYSIM_features_fraud":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/Paysim_features_fraud/train.csv'

        else:
            filenameIn = '../data/Paysim_features_fraud/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat',
                     'errorBalanceOrig':'num',
                     'errorBalanceDest': 'num'}

    elif FLAGS.data_set == "PAYSIM_features_balanced":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/Paysim_features_balanced/train.csv'

        else:
            filenameIn = '../data/Paysim_features_balanced/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat',
                     'errorBalanceOrig':'num',
                     'errorBalanceDest': 'num'}

    elif FLAGS.data_set == "PAYSIM_SMOTE":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/Paysim_SMOTE/train.csv'

        else:
            filenameIn = '../data/Paysim_SMOTE/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}

    elif FLAGS.data_set == "PAYSIM_SMOTE_raw":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/PAYSIM_SMOTE_raw/train.csv'

        else:
            filenameIn = '../data/PAYSIM_SMOTE_raw/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}


    elif FLAGS.data_set == "PAYSIM_raw_balanced":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/PAYSIM_raw_balanced/train.csv'

        else:
            filenameIn = '../data/PAYSIM_raw_balanced/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}

    elif FLAGS.data_set == "PAYSIM_fraud":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/PAYSIM_fraud/train.csv'

        else:
            filenameIn = '../data/PAYSIM_fraud/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}

    elif FLAGS.data_set == "PAYSIM_raw_fraud":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/PAYSIM_raw_fraud/train.csv'

        else:
            filenameIn = '../data/PAYSIM_raw_fraud/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}

    elif FLAGS.data_set == "PAYSIM_raw_normal":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/PAYSIM_raw_normal/train.csv'

        else:
            filenameIn = '../data/PAYSIM_raw_normal/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}

    elif FLAGS.data_set == "PAYSIM_GAN":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/Paysim_GAN/train.csv'

        else:
            filenameIn = '../data/Paysim_GAN/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}



    elif FLAGS.data_set == "PAYSIM_raw_clean":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/PAYSIM_sub_normal/train.csv'

        else:
            filenameIn = '../data/Paysim_normal_clean/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}

    elif FLAGS.data_set == "PAYSIM_sub_raw_normal":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/PAYSIM_sub_raw_normal/train.csv'

        else:
            filenameIn = '../data/PAYSIM_sub_raw_normal/train.csv'

        cols_dict = {'step': 'num',
                     'type': 'cat',
                     'amount': 'num',
                     'oldbalanceOrg': 'num',
                     'newbalanceOrig': 'num',
                     'oldbalanceDest': 'num',
                     'newbalanceDest': 'num',
                     'isFraud': 'cat',
                     'isFlaggedFraud': 'cat'}

    elif FLAGS.data_set == "pca_credit":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/pca_credit/train.csv'

        else:
            filenameIn = '../data/pca_credit/train.csv'

        cols_dict = {'Time': 'num', 'V1': 'num', 'V2': 'num', 'V3': 'num',
                     'V4': 'num', 'V5': 'num', 'V6': 'num', 'V7': 'num', 'V8': 'num',
                     'V9': 'num', 'V10': 'num', 'V11': 'num', 'V12': 'num', 'V13': 'num',
                     'V14': 'num', 'V15': 'num', 'V16': 'num', 'V17': 'num', 'V18': 'num',
                     'V19': 'num', 'V20': 'num', 'V21': 'num', 'V22': 'num', 'V23': 'num',
                     'V24': 'num', 'V25': 'num', 'V26': 'num', 'V27': 'num', 'V28': 'num',
                     'Amount': 'num', 'Class': 'cat'}

    elif FLAGS.data_set == "pca_credit_SMOTE":

        if FLAGS.colab:

            filenameIn = '/content/drive/My Drive/MSc_repo/data/pca_credit_SMOTE/train.csv'

        else:
            filenameIn = '../data/pca_credit_SMOTE/train.csv'

        cols_dict = {'Time': 'num', 'V1': 'num', 'V2': 'num', 'V3': 'num',
                     'V4': 'num', 'V5': 'num', 'V6': 'num', 'V7': 'num', 'V8': 'num',
                     'V9': 'num', 'V10': 'num', 'V11': 'num', 'V12': 'num', 'V13': 'num',
                     'V14': 'num', 'V15': 'num', 'V16': 'num', 'V17': 'num', 'V18': 'num',
                     'V19': 'num', 'V20': 'num', 'V21': 'num', 'V22': 'num', 'V23': 'num',
                     'V24': 'num', 'V25': 'num', 'V26': 'num', 'V27': 'num', 'V28': 'num',
                     'Amount': 'num', 'Class': 'cat'}



    else:

        filenameIn = None
        cols_dict = None
        raise ValueError("Invalid data set")


    use_cols = [item for item in cols_dict]
    cat_cols = [item for item in cols_dict if cols_dict[item] == 'cat']

    # n points to genreate for intermediate results
    n_points = FLAGS.n_points

    if FLAGS.model in ['CondCramer','CondWGAN']:
        print("Making the x_sampler \n")

        x_sampler = LabelledData(file=filenameIn, cat_cols=cat_cols,
                         use_cols=use_cols,label_column = label_column)

    else:
        x_sampler = Data(file=filenameIn, cat_cols=cat_cols,
                         use_cols=use_cols)

    x_dim = x_sampler.shape[1]
    x_size = x_sampler.shape[0]
    num_batches = int(FLAGS.max_epoch * x_size / FLAGS.batch_size)
    z_sampler = NoiseSampler(FLAGS.zDim, FLAGS.batch_size, FLAGS.z_normal)
    # I think this is a formula that gives us a clue and then it can't be smaller than 2 and it can't be bigger
    # than the cardinality of the categorical variable itself
    catEmbeddingDims = [min(max(int(round(FLAGS.k * np.log(v_size))), 2), v_size) for v_size in x_sampler.cenc.cat_card]
    # catEmbeddingDims = None

    if FLAGS.model == "cramer":
        # create model's graph
        d_net = discriminator(dims=discDims, out_dims=FLAGS.discriminatorOut_dims,
                              dims_nCross=FLAGS.discDims_nCross,
                              catEmbeddingDims=catEmbeddingDims, ohFeatureIndices=x_sampler.cenc.feature_indices,
                              numColsLen=len(x_sampler.cenc.num_cols))
        g_net = generator(dims=genDims, out_dims=x_dim, dims_nCross=FLAGS.discDims_nCross,
                              catEmbeddingDims=catEmbeddingDims, ohFeatureIndices=x_sampler.cenc.feature_indices,
                              numColsLen=len(x_sampler.cenc.num_cols))
        cgan = CramerGAN(x_dim=x_dim, z_dim=FLAGS.zDim, g_net=g_net, d_net=d_net, x_sampler=x_sampler, z_sampler=z_sampler)
        # train model
        gen_lossses_, disclosses_ = cgan.train(dirname=generated_data_dir, batch_size=FLAGS.batch_size,
                                               num_batches=num_batches,
                                               interv=FLAGS.interv, decoded_size=n_points, encoded_size=n_points,
                                               models_dir=models_dir)

        # save trained model
        filenameModel = os.path.join(models_dir, "FINAL_model.ckpt")
        saver = tf.train.Saver()
        cgan.saveModel(saver, filenameModel)

        # generate synthetic data
        n_finalGen = max(x_size, n_points)

        if FLAGS.data_set in ['PAYSIM_raw_fraud',"PAYSIM_sub_normal","PAYSIM_sub_raw_normal"]:
            times = int(400000/5000)
            for i in range(times):

                dfout = cgan.sampler(int(n_finalGen / FLAGS.batch_size))
                filename = os.path.join(generated_data_dir, '{} final_generated_data.csv'.format(i))
                dfout.to_csv(filename + ".gz", index=False, compression="gzip",
                             float_format='%.2f')

        else:

            dfout = cgan.sampler(int(n_finalGen / FLAGS.batch_size))
            filename = os.path.join(generated_data_dir, 'final_generated_data.csv')
            dfout.to_csv(filename + ".gz", index=False, compression="gzip",
                         float_format='%.2f')


    elif FLAGS.model == 'WGAN':
        # Create the WGAN graph here
        d_net = discriminator(dims=discDims, out_dims=1, # i.e. for a WGAN we output a scalar
                              dims_nCross=FLAGS.discDims_nCross,
                              catEmbeddingDims=catEmbeddingDims, ohFeatureIndices=x_sampler.cenc.feature_indices,
                              numColsLen=len(x_sampler.cenc.num_cols))
        g_net = generator(dims=genDims, out_dims=x_dim, dims_nCross=FLAGS.discDims_nCross,
                              catEmbeddingDims=catEmbeddingDims, ohFeatureIndices=x_sampler.cenc.feature_indices,
                              numColsLen=len(x_sampler.cenc.num_cols))
        wgan = WGAN(x_dim=x_dim, z_dim=FLAGS.zDim, g_net=g_net, d_net=d_net, x_sampler=x_sampler, z_sampler=z_sampler)

        gen_lossses_, disclosses_ = wgan.train(dirname=generated_data_dir, batch_size=FLAGS.batch_size,
                                               num_batches=num_batches,
                                               interv=FLAGS.interv, decoded_size=n_points, encoded_size=n_points,
                                               models_dir=models_dir)

        # save trained model
        filenameModel = os.path.join(models_dir, "FINAL_model.ckpt")
        saver = tf.train.Saver()
        wgan.saveModel(saver, filenameModel)

        # generate synthetic data
        n_finalGen = max(x_size, n_points)


        dfout = wgan.sampler(int(n_finalGen / FLAGS.batch_size))
        filename = os.path.join(generated_data_dir, 'final_generated_data.csv')
        dfout.to_csv(filename, index=False, float_format='%.2f')

    elif FLAGS.model == 'CondCramer':
        # Create the computation graph for our model here
        d_net = ConditionalDiscriminator(dims=discDims, out_dims=FLAGS.discriminatorOut_dims,
                              dims_nCross=FLAGS.discDims_nCross,
                              catEmbeddingDims=catEmbeddingDims,type = FLAGS.type, ohFeatureIndices=x_sampler.cenc.feature_indices,
                              numColsLen=len(x_sampler.cenc.num_cols))
        g_net = ConditionalGenerator(dims=genDims, out_dims=x_dim, dims_nCross=FLAGS.discDims_nCross,
                          catEmbeddingDims=catEmbeddingDims, ohFeatureIndices=x_sampler.cenc.feature_indices,
                          numColsLen=len(x_sampler.cenc.num_cols),type = FLAGS.type,m = FLAGS.gen_M)
        conditional_cramer = ConditionalCramerGAN(x_dim=x_dim, z_dim=FLAGS.zDim, g_net=g_net, d_net=d_net,
                                                  x_sampler=x_sampler,z_sampler=z_sampler)

        gen_lossses_, disclosses_ = conditional_cramer.train(dirname=generated_data_dir, batch_size=FLAGS.batch_size,
                                               num_batches=num_batches,
                                               interv=FLAGS.interv, decoded_size=n_points, encoded_size=n_points,
                                               models_dir=models_dir)

        # save trained model
        filenameModel = os.path.join(models_dir, "FINAL_model.ckpt")
        saver = tf.train.Saver()
        conditional_cramer.saveModel(saver, filenameModel)

        # generate synthetic data
        n_finalGen = max(x_size, n_points)

        if FLAGS.data_set in ['PAYSIM_raw_balanced']:
            times = int(400000/5000)
            for i in range(times):

                dfout = conditional_cramer.sampler(int(n_finalGen / FLAGS.batch_size))
                filename = os.path.join(generated_data_dir, '{} final_generated_data.csv'.format(i))
                dfout.to_csv(filename + ".gz", index=False, compression="gzip",
                             float_format='%.2f')
        else:
            dfout = conditional_cramer.sampler(int(n_finalGen / FLAGS.batch_size))
            filename = os.path.join(generated_data_dir, 'final_generated_data.csv')
            dfout.to_csv(filename, index=False, float_format='%.2f')

    elif FLAGS.model == 'CondWGAN':
        # Create the computation graph for our model here
        d_net = ConditionalDiscriminator(dims=discDims, out_dims=FLAGS.discriminatorOut_dims,
                              dims_nCross=FLAGS.discDims_nCross,
                              catEmbeddingDims=catEmbeddingDims,type = FLAGS.type, ohFeatureIndices=x_sampler.cenc.feature_indices,
                              numColsLen=len(x_sampler.cenc.num_cols))
        g_net = ConditionalGenerator(dims=genDims, out_dims=x_dim, dims_nCross=FLAGS.discDims_nCross,
                          catEmbeddingDims=catEmbeddingDims, ohFeatureIndices=x_sampler.cenc.feature_indices,
                          numColsLen=len(x_sampler.cenc.num_cols),type = FLAGS.type,m = FLAGS.gen_M)
        conditional_wasserstein = ConditionalWGAN(x_dim=x_dim, z_dim=FLAGS.zDim, g_net=g_net, d_net=d_net,
                                                  x_sampler=x_sampler,z_sampler=z_sampler)

        gen_lossses_, disclosses_ = conditional_wasserstein.train(dirname=generated_data_dir, batch_size=FLAGS.batch_size,
                                               num_batches=num_batches,
                                               interv=FLAGS.interv, decoded_size=n_points, encoded_size=n_points,
                                               models_dir=models_dir)

        # save trained model
        filenameModel = os.path.join(models_dir, "FINAL_model.ckpt")
        saver = tf.train.Saver()
        conditional_wasserstein.saveModel(saver, filenameModel)

        # generate synthetic data
        n_finalGen = max(x_size, n_points)

        dfout = conditional_wasserstein.sampler(int(n_finalGen / FLAGS.batch_size))
        filename = os.path.join(generated_data_dir, 'final_generated_data.csv')
        dfout.to_csv(filename, index=False, float_format='%.2f')



    else:

        raise ValueError("the model that you put into the flags at the top is wrong")

    # Create and save a plot fo the losses to asses the training stability
    training_plot_fname = os.path.join(FLAGS.root_path, "trainingPlots", "{}.png".format(name_experiment))
    plt.plot(gen_lossses_[5:])
    plt.plot(disclosses_[5:])
    plt.xlabel("Training over time")
    plt.ylabel('Losses')
    plt.legend(["Generator", "Discriminator"])
    plt.title("Training stability over time")
    plt.savefig(training_plot_fname, bbox_inches='tight')


if __name__ == '__main__':
    tf.app.run()
    # evaluateResults()