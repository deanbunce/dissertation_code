# Check that 2 data sets have the same cardinality

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

data = "PNR"

if data == "PNR":

    cols_dict = {'Origin_Country': 'cat',
                      'Destination_Country': 'cat',
                      'OfficeIdCountry': 'cat',
                      'SaturdayStay': 'cat',
                      'PurchaseAnticipation': 'num',
                      'NumberPassenger': 'cat',
                      'StayDuration': 'num',
                      'Title': 'cat',
                      'TravelingWithChild': 'cat',
                      'ageAtPnrCreation': 'num',
                      'Nationality2': 'cat',
                      'BL': 'cat'}

else: cols_dict = None

train_data = pd.read_csv("../data/processedAirplane/train.csv")
test_data = pd.read_csv("../data/processedAirplane/test.csv")

use_cols = [item for item in cols_dict]
cat_cols = [item for item in cols_dict if cols_dict[item] == 'cat']
def data_check(train,test):
    use_data = True
    for c in cat_cols:
        tr = train[c].unique()
        te = test[c].unique()
        if len(tr) == len(te):
            a = (np.sort(tr)==np.sort(te))
            fine = all(a)

            if fine:
                pass
            else:
                use_data = False


        else:
            print("Not working "+c)
            use_data = False
            print(te)
            print(tr)

    return use_data


# #
# df = pd.read_csv("../data/Airplane/out-2Cross.csv")
# print(df["NumberPassenger"].astype(str).value_counts(normalize=False).head(15))
#
# df.loc[df["NumberPassenger"]==10,"NumberPassenger"]-=1
#
#
# print(df["NumberPassenger"].astype(str).value_counts(normalize=False).head(15))
#
# tr, te = train_test_split(df, test_size=0.3)
#
# data_check(tr,te)


