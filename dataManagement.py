# Model to get a data set from a csv format into a format that can be processed by a GAN that creates data
# with real-valued and categorical features
# Based on Alejandro Mottini  Alix Lhe ́ritier  Rodrigo Acuna-Agost's code encode_decode.py (included in this repo)
# This pretty much just gives us the data in the format that we need for training
###############
import pandas as pd
from sklearn.preprocessing import LabelEncoder,StandardScaler
from sklearn.feature_extraction import DictVectorizer
from sklearn.preprocessing import MinMaxScaler,OneHotEncoder
from sklearn.model_selection import train_test_split
import numpy as np
import os
from sklearn import preprocessing
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix,  accuracy_score
from sklearn.tree import DecisionTreeClassifier
###############
class Data:

    def __init__(self, file, cat_cols, use_cols=None, nrows=None):

        _, file_extension = os.path.splitext(file)

        if file_extension == ".csv":
            df = pd.read_csv(file, usecols=use_cols, nrows=nrows)

        else:
            df = None
            print("wrong file format: " + file_extension)
            exit(1)

        self.cenc = CatEncoder(cat_cols)
        self._data = self.cenc.fit_transform(df)
        self.shape = self._data.shape
        self._epochs_completed = 0
        self._index_in_epoch = 0
        self._num_examples = self._data.shape[0]

        # Shuffle the data
        perm = np.arange(self._num_examples)
        np.random.shuffle(perm)
        self._data = self._data[perm]
        print(self._data.shape)

    def __call__(self, batch_size):
        """Return the next `batch_size` examples from this data set."""

        start = self._index_in_epoch
        self._index_in_epoch += batch_size
        if self._index_in_epoch > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Shuffle the data
            perm = np.arange(self._num_examples)
            np.random.shuffle(perm)
            self._data = self._data[perm]
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size

            assert batch_size <= self._num_examples
        end = self._index_in_epoch
        return self._data[start:end]



class LabelledData(object):

    def __init__(self, file, cat_cols, use_cols=None, nrows=None,label_column = None):

        _, file_extension = os.path.splitext(file)

        if file_extension == ".csv":
            print("File reading in \n")
            df = pd.read_csv(file, usecols=use_cols, nrows=nrows)
            print("File read in \n")

        else:
            df = None
            print("wrong file format: " + file_extension)
            exit(1)

        print(df.shape)
        self.cenc = CatEncoder(cat_cols)
        print("Doing the encoder transform ... \n")
        self._data = self.cenc.fit_transform(df)
        self.shape = self._data.shape
        print("Doing the encoder transform ... \n")
        self._epochs_completed = 0
        self._index_in_epoch = 0
        self._num_examples = self._data.shape[0]

        # Before we shuffles the data, we must make the labels
        if label_column == 'BL':

            self._labels = np.array((df.BL == 'L')*1).reshape(self._data.shape[0],1)
            self.p = np.mean(self._labels)

        elif label_column == 'isFraud':
            print("getting the fraud variables")
            self._labels = np.array(df.isFraud.values).reshape(self._data.shape[0],1)
            print('getting the mean')
            self.p = 0.001290820448180152 #hardcoded bc it was taking an actual day to calculate

        elif label_column == 'Class':
            print("getting the fraud variables")
            self._labels = np.array(df.Class.values).reshape(self._data.shape[0],1)
            print('getting the mean')
            self.p = 0.00172


        else:

            raise ValueError("The error is in the label data class, you need too pass it sme labels")

        # Shuffle the data
        print("shuffling")

        perm = np.arange(self._num_examples)
        print("Busy ... \n")
        np.random.shuffle(perm)
        self._data = self._data[perm]
        self._labels = self._labels[perm]
        print(self._data.shape)
        print("the shape of the labels is {}".format(self._labels.shape))



    def __call__(self, batch_size):
        """Return the next `batch_size` examples with their labels from this data set."""

        start = self._index_in_epoch
        self._index_in_epoch += batch_size
        if self._index_in_epoch > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Shuffle the data and the labels
            perm = np.arange(self._num_examples)
            np.random.shuffle(perm)
            self._data = self._data[perm]
            self._labels = self._labels[perm]
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size

            assert batch_size <= self._num_examples
        end = self._index_in_epoch
        """Now we returning 2 things, its good!"""

        return self._data[start:end], self._labels[start:end]




class CatEncoder:

    def __init__(self, cat_cols):
        self.feature_indices = None
        self.vectorizer = None
        self.cat_cols = cat_cols
        self.cat_card = None
        self.num_cols = None  # set in fit_transform
        self.columns = None  # set in fit_transform to save original column order
        self.mm = None
        self.le = None
        self.num_cols_with_nulls = []

    def transform(self, df, original_cat_cols):

        # leave original
        df = df.copy()

        # Handling the null columns, we will not need this for the paysim data
        for c in (set(df.columns) - set(original_cat_cols)):
            if df[c].isnull().any():
                newcol = c + "_isnull"
                df[newcol] = "0"
                df.loc[df[c].isnull().values, newcol] = "1"
                df[c].fillna(df[c].mean(), inplace=True)

        df[self.cat_cols] = df[self.cat_cols].applymap(str)
        vectorized = self.vectorizer.transform(df[self.cat_cols].to_dict(orient='records'))
        print(vectorized)

        # compute feature_indices (as in oneHotEncoder, except that it doesn't contain the starting position) for inverse_transform
        columns = [s.split('=')[0] for s in sorted(self.vectorizer.vocabulary_.keys())]
        columns = self.le.transform(columns)

        nummat = df.drop(self.cat_cols, axis=1)
        nummat = self.mm.transform(nummat)

        vectorized = np.concatenate((nummat, vectorized), axis=1)
        print(vectorized)

        return vectorized

    # This whole function is there to make the categorical columns equals to one-hot vector and makes the
    # other column between 0 and 1 using the minmax scaler

    def fit_transform(self, df):

        for c in (set(df.columns) - set(self.cat_cols)):
            if df[c].isnull().any():
                print(c + " contains null")
                newcol = c + "_isnull"
                df[newcol] = "0"
                df.loc[df[c].isnull().values, newcol] = "1"
                self.cat_cols.append(newcol)
                df[c].fillna(df[c].mean(), inplace=True)
                self.num_cols_with_nulls.append(c)

        self.columns = df.columns
        df[self.cat_cols] = df[self.cat_cols].applymap(str)
        self.cat_card = df[self.cat_cols].apply(lambda x: np.unique(x).size)
        self.vectorizer = DictVectorizer(sparse=False)
        vectorized = self.vectorizer.fit_transform(df[self.cat_cols].to_dict(orient='records'))

        # compute feature_indices (as in oneHotEncoder, except that it doesn't contain the starting position) for inverse_transform
        columns = [s.split('=')[0] for s in sorted(self.vectorizer.vocabulary_.keys())]
        self.le = LabelEncoder()
        columns = self.le.fit_transform(columns)
        self.feature_indices = np.where(columns[:-1] != columns[1:])[0] + 1
        self.feature_indices = np.append(self.feature_indices, vectorized.shape[1])

        # numerical columns
        nummat = df.drop(self.cat_cols, axis=1)
        self.num_cols = nummat.columns
        self.mm = MinMaxScaler()
        nummat = self.mm.fit_transform(nummat)

        vectorized = np.concatenate((nummat, vectorized), axis=1)

        return vectorized

    # DECODE
    def inverse_transform(self, mat):
        nummat = mat[:, :len(self.num_cols)]
        ohmat = mat[:, len(self.num_cols):]

        # correct not properly one-hot encoded data by assigning 1 to largest value and 0 to the rest
        start = 0
        for end in self.feature_indices:
            view = ohmat[:, start:end]
            c_argmax = np.argmax(view, axis=1)[:, np.newaxis]
            b_map1 = c_argmax == np.arange(view.shape[1])
            view[b_map1] = 1
            view[~b_map1] = 0
            start = end

        print("Turning one_hot vectors into categorical features")
        recovered = self.vectorizer.inverse_transform(ohmat)
        print("formatting back into a data frame")
        dfRecovered = pd.DataFrame.from_dict([list_to_dict(r.keys()) for r in recovered])
        print("Inverting the inverse transformation")
        nummat = self.mm.inverse_transform(nummat)
        dfNum = pd.DataFrame(nummat, columns=self.num_cols)
        ret = pd.concat([dfRecovered, dfNum], axis=1, ignore_index=False)

        # reorder
        ret = ret[self.columns]
        for c in self.num_cols_with_nulls:
            newcol = c + "_isnull"
            ret.loc[ret[newcol] == "1", c] = np.nan
            ret.drop(newcol, axis=1, inplace=True)

        return ret

############


def list_to_dict(rlist):
    return dict(map(lambda s : s.split('='), rlist))


def preprocessData_model(dataIn, cols_featuresNum, cols_toOneHot, col_target):
    data_list = []
    cols_out = []

    # numerical data
    numEnc = None
    if len(cols_featuresNum) > 0:
        data_numerical = dataIn[cols_featuresNum].astype(np.float).values

        # Choice of the standard scaler or the minmax scaler
        num_preP_type = 'StdScaler'
        if num_preP_type == 'MinMax':
            numEnc = MinMaxScaler()
        else:
            numEnc = StandardScaler()
        data_numerical = numEnc.fit_transform(data_numerical)

        data_list.append(data_numerical)
        cols_out = cols_out + cols_featuresNum

    # categorical data: one hot
    le_list_OH = []
    oneHotEnc = None
    colsOH = []
    if len(cols_toOneHot) > 0:
        data_cat = dataIn[cols_toOneHot].astype(np.str).values
        for colN in range(len(cols_toOneHot)):
            unique_vals = np.unique(data_cat[:, colN])
            le = LabelEncoder()
            le.fit(unique_vals)
            data_cat[:, colN] = le.transform(data_cat[:, colN])
            le_list_OH.append(le)

        oneHotEnc = OneHotEncoder(handle_unknown='ignore', sparse=False)  # ignore if new value in test set
        data_oh = oneHotEnc.fit_transform(data_cat)
        colsOH = []
        # f_index=oneHotEnc.feature_indices_
        for i, colName in enumerate(cols_toOneHot):
            le = le_list_OH[i]
            # i_0=f_index[i]
            # i_end=f_index[i+1]
            # colsClass=[colName+'_'+str(le.inverse_transform(t)) for t in range(i_end-i_0) ]
            # colsOH=colsOH+colsClass

        data_list.append(data_oh)
        # cols_out=cols_out+colsOH

    data_np = np.concatenate(data_list, axis=1)  # by columns
    target_preP = dataIn[col_target].values
    le_target = preprocessing.LabelEncoder()
    target = le_target.fit_transform(target_preP)

    return data_np, target, numEnc, le_list_OH, oneHotEnc, colsOH, le_target


