# File to do exploratory data analysis on the credit card data set
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler,StandardScaler
from sklearn.model_selection import train_test_split
import struct
from dataCheck import data_check
import imblearn.over_sampling as imb
import os

# Read the data in as a data frame
credit_card_data = pd.read_csv("../data/PS_20174392719_1491204439457_log.csv")
cols = credit_card_data.columns
which_data = "PAYSIM_GAN"  # Argument to prevent multiple data sets being created each time the data is run

# Shuffle data so subsets can have a reasonable number of fraud cases
shuffled_credit_card_data = credit_card_data.sample(frac=1, random_state=247).reset_index(drop=True)


# Doing the preprocessing for the airplane data
if which_data == "PNR":
    data = pd.read_csv("../data/Airplane/out-2Cross.csv")
    data2 = data.copy()
    data2.loc[data["NumberPassenger"] == 10, "NumberPassenger"] = data2.loc[data["NumberPassenger"] == 10, "NumberPassenger"] - 1
    count = 0
    use_data = False
    while not use_data:
        count+=1
        tr,te = train_test_split(data2, test_size=0.3)
        use_data = data_check(tr,te)
        print("Use_data is {} at attempt #{}".format(use_data,count))

    train,test = tr,te
    train.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/processedAirplane/train.csv')
    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/processedAirplane/test.csv')


if which_data == "PAYSIM":
    data = pd.read_csv("../data//PS_20174392719_1491204439457_log.csv")
    cols = ['step', 'type' ,'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud','isFlaggedFraud']
    data = data.loc[:,cols]
    log_cols = ['step','amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest']

    for i, name in enumerate(log_cols):
        d0 = np.log10(data[name].values + 1)
        # d0 = np.log1p( data['Amount'].values ) / np.log(10)
        data[name] = d0
        print(data[name].head(15))
    train, test = train_test_split(data, test_size=0.3)
    print(train.head(15))
    train.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim/train.csv')
    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim/test.csv')
    data.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim/full_data.csv')


    print(train.shape)

if which_data == "PAYSIM_raw":
    data = pd.read_csv("../data//PS_20174392719_1491204439457_log.csv")
    cols = ['step', 'type' ,'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud','isFlaggedFraud']
    data = data.loc[:,cols]
    train, test = train_test_split(data, test_size=0.3)
    print(train.head(15))
    train.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_raw/train.csv')
    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_raw/test.csv')

    print(train.shape)

if which_data == "PAYSIM_raw_balanced":

    train = pd.read_csv('../data/Paysim_raw/train.csv')
    test = pd.read_csv('../data/Paysim_raw/test.csv')

    prop_fraud = 0.001290820448180152
    # Split the training data
    train_fraud_data = train.copy()
    train_fraud_data = train_fraud_data.loc[train_fraud_data.isFraud == 1, :]
    train_normal_data = train.copy()
    train_normal_data = train_normal_data.loc[train_normal_data['isFraud'] == 0, :]


    small_data = train_normal_data.sample(frac=2*prop_fraud, replace=False)
    train = pd.concat([small_data, train_fraud_data])

    print(train.head(15))
    train.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_raw_balanced/train.csv')
    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_raw_balanced/test.csv')

    print(train.shape)

if which_data == "PAYSIM_raw_fraud":

    train = pd.read_csv('../data/Paysim_raw/train.csv')
    test = pd.read_csv('../data/Paysim_raw/test.csv')

    train_fraud_data = train.copy()
    train_fraud_data = train_fraud_data.loc[train_fraud_data['isFraud'] == 1, :]
    test_fraud_data = test.copy()
    test_fraud_data = test_fraud_data.loc[test_fraud_data['isFraud'] == 1, :]


    print(train_fraud_data.head(15))
    print(test_fraud_data.head(15))


    train_fraud_data.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_raw_fraud/train.csv')
    test_fraud_data.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_raw_fraud/test.csv')

    print(train.shape)

if which_data == "PAYSIM_raw_normal":

    train = pd.read_csv('../data/Paysim_raw/train.csv')
    test = pd.read_csv('../data/Paysim_raw/test.csv')
    train_normal_data = train.copy()
    train_normal_data = train_normal_data.loc[train_normal_data.isFraud == 0, :]
    test_normal_data = test.copy()
    test_normal_data = test_normal_data.loc[test_normal_data.isFraud == 0, :]

    print(train_normal_data.head(15))
    print(train_normal_data.shape)
    train_normal_data.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_raw_normal/train.csv')
    test_normal_data.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_raw_normal/test.csv')

if which_data == "PAYSIM_raw_2GAN":

    fraud = pd.read_csv("/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/generated_data/cramer__PAYSIM_raw_fraud_epochs_20000_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_14:39:51,14M2019-07-14_genM_-1/45 final_generated_data.csv.gz")
    normal = pd.read_csv("/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/generated_data/cramer__PAYSIM_raw_normal_epochs_45_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_21:52:21,14M2019-07-14_genM_-1/final_generated_data.csv.gz")

    both = pd.concat([fraud,normal])

    both.to_csv("/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/generated_data/2GAN/data.csv")

    print(both.shape)



if which_data == "PAYSIM_sub_normal":

    train = pd.read_csv('../data/Paysim/train.csv')
    test = pd.read_csv('../data/Paysim/test.csv')
    train_normal_data = train.copy()
    train_normal_data = train_normal_data.loc[train_normal_data.isFraud == 0, :]
    train_normal_data = train_normal_data.sample(9000)
    test_normal_data = test.copy()
    test_normal_data = test_normal_data.loc[test_normal_data.isFraud == 0, :]

    print(train_normal_data.head(15))
    print(train_normal_data.shape)
    train_normal_data.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_sub_normal/train.csv')
    test_normal_data.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_sub_normal/test.csv')



if which_data == "PAYSIM_SMOTE":

    data = pd.read_csv("../data/PS_20174392719_1491204439457_log.csv")
    train = pd.read_csv("../data/Paysim/train.csv")
    test = pd.read_csv("../data/Paysim/test.csv")

    # data = pd.read_csv("/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/PS_20174392719_1491204439457_log.csv")

    cols = ['step', 'type', 'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud',
            'isFlaggedFraud']
    xcols = ['step',  'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest',
            'isFlaggedFraud', 'use']


    # Split into training and test sets
    summary_real = train.loc[:,'type'].astype(str).value_counts(normalize=True)

    # From the console
    keys = summary_real.keys()
    proportions = np.array(summary_real.values).reshape(5,1)
    bounds = np.cumsum(proportions)
    bounds = np.concatenate([[0], bounds]).reshape(6, 1)
    ranges = np.array([bounds[i + 1] - bounds[i] for i in range(5)]).reshape(5, 1)
    sds = ranges / 8
    midpoints = np.array([(bounds[i + 1] - bounds[i]) / 2 + bounds[i] for i in range(5)]).reshape(5, 1)

    for i in range(5):
        train[keys[i]] = np.random.normal(loc=midpoints[i], scale=sds[i], size=train.shape[0])

    for i in range(5):
        train.loc[data['type'] == keys[i], 'use'] = train.loc[data['type'] == keys[i], keys[i]]

    train = train.drop(keys, axis=1)

    train = train.drop('type', axis=1)

    X = train[xcols]
    y = train['isFraud']

    sm = imb.SMOTE(random_state=42,k_neighbors=2)
    X_res, y_res = sm.fit_resample(X, y)
    #
    train_smote = pd.DataFrame(X_res,columns = xcols)
    train_smote['isFraud'] = y_res

    cat_back = 0
    for i in range(5):
        cat_back += (train_smote['use'].values > bounds[i]) * (i)

    train_smote['type'] = cat_back


    cleanup_nums = {"type": {0: keys[0], 1: keys[1], 3:keys[2], 6:keys[3],10:keys[4]},}
    train_smote.replace(cleanup_nums, inplace=True)

    print(train_smote.head(10))
    print(train_smote.shape)

    train_smote_out = train_smote.sample(frac=0.5)
    print(train.head(15))
    train_smote_out.to_csv(
        '/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_SMOTE/train.csv')
    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_SMOTE/test.csv')

    print(train_smote_out.shape)
    print(train_smote_out.step)

if which_data == "PAYSIM_SMOTE_raw":

    data = pd.read_csv("../data/PS_20174392719_1491204439457_log.csv")
    train = pd.read_csv("../data/Paysim_SMOTE/train.csv")
    test = pd.read_csv("../data/Paysim_SMOTE/test.csv")

    # data = pd.read_csv("/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/PS_20174392719_1491204439457_log.csv")

    cols = ['step', 'type', 'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud',
            'isFlaggedFraud']

    xcols = ['step',  'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest',
            'isFlaggedFraud', 'use']
    log_cols = ['step','amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest']


    for col in log_cols:

        d0 = (10**train[col].values - 1)
        train[col] = d0

        d1 = (10**test[col].values - 1)
        test[col] = d1


    train.to_csv(
        '/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_SMOTE_raw/train.csv')

    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_SMOTE_raw/test.csv')



if which_data == "PAYSIM_balanced":

    data = pd.read_csv("../data//PS_20174392719_1491204439457_log.csv")
    train = pd.read_csv("../data/Paysim/train.csv")
    test = pd.read_csv("../data/Paysim/train.csv")
    cols = ['step', 'type' ,'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud','isFlaggedFraud']
    data = data.loc[:,cols]
    train = train.loc[:,cols]
    test = test.loc[:,cols]

    prop_fraud = np.sum(data.isFraud == 1) / data.shape[0]
    # Split the training data
    train_fraud_data = train.copy()
    train_fraud_data = train_fraud_data.loc[train_fraud_data.isFraud == 1, :]
    train_normal_data = train.copy()
    train_normal_data = train_normal_data.loc[train_normal_data['isFraud'] == 0, :]


    small_data = train_normal_data.sample(frac=1*prop_fraud, replace=False)
    train = pd.concat([small_data, train_fraud_data])
    print(data.shape)

    print(train.head(15))
    train.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_balanced/train.csv')
    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_balanced/test.csv')

    print(train.shape)

if which_data == "PAYSIM_fraud":

    data = pd.read_csv("../data//PS_20174392719_1491204439457_log.csv")
    train = pd.read_csv("../data/Paysim/train.csv")
    test = pd.read_csv("../data/Paysim/test.csv")
    cols = ['step', 'type' ,'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud','isFlaggedFraud']
    data = data.loc[:,cols]
    # Split into training and test sets
    train_fraud = train.copy()
    train_fraud = train_fraud.loc[train_fraud.isFraud == 1, :]
    test_fraud = test.copy()
    test_fraud = test_fraud.loc[test_fraud.isFraud == 1, :]
    train_fraud.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_fraud/train.csv')
    test_fraud.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_fraud/test.csv')
    print(train.shape)

if which_data == "PAYSIM_GAN":

    train = pd.read_csv("../data/Paysim_raw/train.csv")
    test = pd.read_csv("../data/Paysim_raw/train.csv")

    fraud_data = None
    for i in range(80):

        if fraud_data is None:

            data_path = os.path.join("../generated_data/cramer__PAYSIM_raw_fraud_epochs_20000_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_14:39:51,14M2019-07-14_genM_-1",
                                '{} final_generated_data.csv.gz'.format(i))
            fraud_data = pd.read_csv(data_path)

        else:

            data_path = os.path.join("../generated_data/cramer__PAYSIM_raw_fraud_epochs_20000_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_14:39:51,14M2019-07-14_genM_-1",
                                     '{} final_generated_data.csv.gz'.format(i))
            new_data = pd.read_csv(data_path)
            print("Shape of new data:{}".format(new_data.shape))
            print(new_data.columns)
            fraud_data = pd.concat([fraud_data,new_data],axis = 0)
            print(fraud_data.shape)

    train_normal_data = train.copy()
    train_normal_data = train_normal_data.loc[train_normal_data['isFraud'] == 0, :]
    print("The shape of the normal data is: {}".format(train_normal_data.shape))

    train_fraud_data = train.copy()
    train_fraud_data = train_fraud_data.loc[train_fraud_data['isFraud'] == 1, :]
    print("The shape of the fraud data is: {}".format(train_fraud_data.shape))

    small_data = train_normal_data.sample(n=400000, replace=False)
    train = pd.concat([small_data, train_fraud_data,fraud_data],axis = 0)
    print("Train shape is {}".format(train.shape))

    cols = ['step', 'type' ,'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud','isFlaggedFraud']
    train = train.loc[:,cols]
    test = test.loc[:,cols]



    print(train.loc[train['isFraud'] == 1, :].shape)
    print(train.loc[train['isFraud'] == 0, :].shape)

    print(train.columns)
    train.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_GAN/train.csv')
    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_GAN/test.csv')

################################################################################################################
################################################################################################################
################################################################################################################
################################################################################################################
################################################################################################################
################################################################################################################
################################################################################################################
################################################################################################################

if which_data == "PAYSIM_features_balanced":

    data = pd.read_csv("../data//PS_20174392719_1491204439457_log.csv")
    cols = ['step', 'type' ,'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud','isFlaggedFraud']
    data = data.loc[:,cols]
    log_cols = ['step','amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest']

    prop_fraud = np.sum(data.isFraud == 1) / data.shape[0]

    for i, name in enumerate(log_cols):
        d0 = np.log10(data[name].values + 1)
        data[name] = d0
        print(data[name].head(15))

    data['errorBalanceOrig'] = data.newbalanceOrig + data.amount - data.oldbalanceOrg
    data['errorBalanceDest'] = data.oldbalanceDest + data.amount - data.newbalanceDest
    # Split into training and test sets
    train, test = train_test_split(data, test_size=0.3)

    # Split the training data
    train_fraud_data = train.copy()
    train_fraud_data = train_fraud_data.loc[train_fraud_data.isFraud == 1, :]
    train_normal_data = train.copy()
    train_normal_data = train_normal_data.loc[train_normal_data['isFraud'] == 0, :]


    small_data = train_normal_data.sample(frac=1*prop_fraud, replace=False)
    train = pd.concat([small_data, train_fraud_data])
    print(data.shape)

    print(train.head(15))
    train.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_features_balanced/train.csv')
    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_features_balanced/test.csv')

    print(train.shape)

if which_data == "PAYSIM_features_GAN":

    data = pd.read_csv("../data//PS_20174392719_1491204439457_log.csv")
    data['errorBalanceOrig'] = data.newbalanceOrig + data.amount - data.oldbalanceOrg
    data['errorBalanceDest'] = data.oldbalanceDest + data.amount - data.newbalanceDest
    train, test = train_test_split(data, test_size=0.3)

    fraud_data = None
    for i in range(80):

        if fraud_data is None:

            data_path = os.path.join("../generated_data/cramer__PAYSIM_features_fraud_epochs_10000_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_20:23:28,03M2019-07-03_genM_0_discM_0",
                                '{} final_generated_data.csv.gz'.format(i))
            fraud_data = pd.read_csv(data_path)

        else:

            data_path = os.path.join("../generated_data/cramer__PAYSIM_features_fraud_epochs_10000_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_20:23:28,03M2019-07-03_genM_0_discM_0",
                                     '{} final_generated_data.csv.gz'.format(i))
            new_data = pd.read_csv(data_path)
            print("Shape of new data:{}".format(new_data.shape))
            print(new_data.columns)
            fraud_data = pd.concat([fraud_data,new_data],axis = 0)
            print(fraud_data.shape)



    train_normal_data = train.copy()
    train_normal_data = train_normal_data.loc[train_normal_data['isFraud'] == 0, :]
    print("The shape of the normal data is: {}".format(train_normal_data.shape))

    train_fraud_data = train.copy()
    train_fraud_data = train_fraud_data.loc[train_fraud_data['isFraud'] == 1, :]
    print("The shape of the fraud data is: {}".format(train_fraud_data.shape))

    small_data = train_normal_data.sample(n=400000, replace=False)
    train = pd.concat([small_data, train_fraud_data,fraud_data],axis = 0)
    print("Train shape is {}".format(train.shape))

    cols = ['step', 'type' ,'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud','isFlaggedFraud','errorBalanceOrig','errorBalanceDest']
    train = train.loc[:,cols]
    test = test.loc[:,cols]
    log_cols = ['step','amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest','errorBalanceOrig','errorBalanceDest']

    for i, name in enumerate(log_cols):
        d0 = np.log10(train[name].values + 1)
        train[name] = d0

    for i, name in enumerate(log_cols):
        d0 = np.log10(test[name].values + 1)
        test[name] = d0

    print(train.loc[train['isFraud'] == 1, :].shape)
    print(train.loc[train['isFraud'] == 0, :].shape)

    print(train.columns)

    train = train[np.isfinite(train['errorBalanceOrig'])]
    train = train[np.isfinite(train['errorBalanceDest'])]


    train.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_features_GAN/train.csv')
    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_features_GAN/test.csv')


if which_data == "PAYSIM_features":
    data = pd.read_csv("../data//PS_20174392719_1491204439457_log.csv")
    cols = ['step', 'type' ,'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud','isFlaggedFraud']
    data = data.loc[:,cols]
    log_cols = ['step','amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest']

    for i, name in enumerate(log_cols):
        d0 = np.log10(data[name].values + 1)
        data[name] = d0
        print(data[name].head(15))
    data['errorBalanceOrig'] = data.newbalanceOrig + data.amount - data.oldbalanceOrg
    data['errorBalanceDest'] = data.oldbalanceDest + data.amount - data.newbalanceDest
    train, test = train_test_split(data, test_size=0.3)
    print(train.head(15))
    train.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_features/train.csv')
    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_features/test.csv')
    data.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_features/full_data.csv')
    print(train.shape)

if which_data == "PAYSIM_features_fraud":
    data = pd.read_csv("../data//PS_20174392719_1491204439457_log.csv")
    cols = ['step', 'type' ,'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud','isFlaggedFraud']
    data = data.loc[:,cols]
    log_cols = ['step','amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest']

    for i, name in enumerate(log_cols):
        d0 = np.log10(data[name].values + 1)
        data[name] = d0
        print(data[name].head(15))

    data['errorBalanceOrig'] = data.newbalanceOrig + data.amount - data.oldbalanceOrg
    data['errorBalanceDest'] = data.oldbalanceDest + data.amount - data.newbalanceDest

    data_fraud = data.copy()
    data_fraud = data_fraud.loc[data_fraud.isFraud == 1, :]

    train, test = train_test_split(data_fraud, test_size=0.3)
    print(train.head(15))
    train.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_features_fraud/train.csv')
    test.to_csv('/Users/deanbunce/Documents/UCL/Term 3/Project/MSC_repo/msc-dissertation/data/Paysim_features_fraud/test.csv')
    print(train.shape)



# Doing the preprocessing step for the MNIST data
# reading in and assignment of the data
def read_idx(filename: object) -> object:
    with open(filename, 'rb') as f:
        zero, data_type, dims = struct.unpack('>HBB', f.read(4))
        shape = tuple(struct.unpack('>I', f.read(4))[0] for d in range(dims))
        return np.fromstring(f.read(), dtype=np.uint8).reshape(shape)

if which_data == 'MNIST':
    test_data = read_idx('../data/MNIST/t10k-images.idx3-ubyte')
    test_data_labels = read_idx('../data/MNIST/t10k-labels.idx1-ubyte')
    training_data_labels = read_idx('../data/MNIST/train-labels.idx1-ubyte')
    training_data = read_idx('../data/MNIST/train-images.idx3-ubyte')

    # get only 1 training example
    training_set = np.array(training_data).reshape(60000, 784, 1) / 255
    data_size = 60000
    train_digits = training_set[0:data_size, :, :]


# This is for the full adult cenus data
if which_data == 'adult_census':
    adult_census_data = pd.read_csv('../data/AdultCensus/adult.data', sep=',',
                                    header=None)

    # Assign the data to the read-in data
    data = adult_census_data
    data = data[(data != ' ?').all(axis=1)]
    data.columns = [
        "Age", "WorkClass", "fnlwgt", "Education", "EducationNum",
        "MaritalStatus", "Occupation", "Relationship", "Race", "Gender",
        "CapitalGain", "CapitalLoss", "HoursPerWeek", "NativeCountry", "Income"
    ]

    # Code the income as a binary variable in order to make this a parallel use case
    # to the PNR data
    # data["Income"] = data["Income"].map({" <=50K": 0.0, " >50K": 1.0})
    data.loc[data["Income"]==" <=50K","Income"] = 0.0
    data.loc[data["Income"]==" >50K","Income"] = 1.0

    # Make all the numerical categories floats
    data.Age = data.Age.astype(float)
    data.fnlwgt = data.fnlwgt.astype(float)
    data.EducationNum = data.EducationNum.astype(float)
    data.HoursPerWeek = data.HoursPerWeek.astype(float)
    # Get 1 hot encoding for the categorical features
    data = pd.get_dummies(data, columns=["WorkClass", "Education",
                                         "MaritalStatus", "Occupation", "Relationship", "Race", "Gender",
                                         "NativeCountry"])

    # Now we scale the numerical columns to be in the range (0,1)
    numerical_cols = ['Age', 'fnlwgt', 'EducationNum',"CapitalGain", "CapitalLoss", 'HoursPerWeek']
    scaler = MinMaxScaler(feature_range=(0, 1))
    scaled_data = scaler.fit_transform(data[numerical_cols])
    data[numerical_cols] = scaled_data
    numerical_cols_cat = ['Age', 'fnlwgt', 'EducationNum',"CapitalGain", "CapitalLoss", 'HoursPerWeek',"Income"]
    num_data = data[numerical_cols_cat]

    num_data.to_pickle('../data/adult_census_numerical.pkl')
    data.to_pickle('../data/adult_census.pkl')


if which_data == "adult_test":
    adult_census_data = pd.read_csv('../data/AdultCensus/adult.test', sep=',',
                                    header=None)

    # Assign the data to the read-in data
    data = adult_census_data
    data = data[(data != ' ?').all(axis=1)]
    data.columns = [
        "Age", "WorkClass", "fnlwgt", "Education", "EducationNum",
        "MaritalStatus", "Occupation", "Relationship", "Race", "Gender",
        "CapitalGain", "CapitalLoss", "HoursPerWeek", "NativeCountry", "Income"
    ]

    # Code the income as a binary variable in order to make this a parallel use case
    # to the PNR data
    print(data["Income"])
    # data["Income"] = data["Income"].map({" <=50K": 0.0, " >50K": 1.0})

    data.loc[data["Income"]==" <=50K.","Income"] = 0.0
    data.loc[data["Income"]==" >50K.","Income"] = 1.0

    # Make all the numerical categories floats
    data.Age = data.Age.astype(float)
    data.fnlwgt = data.fnlwgt.astype(float)
    data.EducationNum = data.EducationNum.astype(float)
    data.HoursPerWeek = data.HoursPerWeek.astype(float)
    # Get 1 hot encoding for the categorical features
    data = pd.get_dummies(data, columns=["WorkClass", "Education",
                                         "MaritalStatus", "Occupation", "Relationship", "Race", "Gender",
                                         "NativeCountry"])

    # Now we scale the numerical columns to be in the range (0,1)
    numerical_cols = ['Age', 'fnlwgt', 'EducationNum', "CapitalGain", "CapitalLoss", 'HoursPerWeek']
    # scaler = MinMaxScaler(feature_range=(0, 1))
    # scaled_data = scaler.fit_transform(data[numerical_cols])
    # data[numerical_cols] = scaled_data
    numerical_cols_cat = ['Age', 'fnlwgt', 'EducationNum',"CapitalGain", "CapitalLoss", 'HoursPerWeek',"Income"]
    num_data = data[numerical_cols_cat]

    num_data.to_pickle('../data/adult_test_numerical.pkl')
    data.to_pickle('../data/adult_test.pkl')
