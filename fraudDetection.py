# Script try and reproduce the kaggle results and get some preliminary scores from TestGAN and TrainGAN

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
from sklearn.model_selection import train_test_split, learning_curve
from sklearn.metrics import average_precision_score
from xgboost.sklearn import XGBClassifier
from xgboost import plot_importance #, to_graphviz
import os
from dataManagement import preprocessData_model
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix,  accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score as auroc

# import shap
# # initialize the shap things
#
# shap.initjs()


# From the exploratory data analysis (EDA) of section 2, we know that fraud only occurs in 'TRANSFER's and 'CASH_OUT's. So we assemble only the corresponding data in X for analysis.

def main_run(which_fake_data,all=False,test_type = "GAN_test",  use_case = "PAYSIM",colab = False,raw = False):


    # Make sure we are doing a legit test
    assert test_type in ["GAN_train","GAN_test","real"]
    assert use_case in ["PNR","PAYSIM","PAYSIM_features","PAYSIM_balanced","PAYSIM_raw","pca_credit"]

    if colab:
        root_path = "/content/drive/My Drive/MSc_repo/"
    else:
        root_path = "../"


    if use_case == "PNR":

        which_fake_data = which_fake_data
        which_test_data = "data/processedAirplane/test_evaluation.csv"
        which_train_data = "data/processedAirplane/train_evaluation.csv"

        fake_data_path = which_fake_data
        test_data_path = os.path.join(root_path, which_test_data)
        train_data_path = os.path.join(root_path, which_train_data)

        df_fake = pd.read_csv(fake_data_path)
        df_test = pd.read_csv(test_data_path)
        df_train = pd.read_csv(train_data_path)

        print(df_fake.shape)
        print(df_test.shape)
        print(df_train.shape)

        # sample a subset of the generated data so that it is the same size as the test set
        nsubs = min(df_fake.shape[0], df_test.shape[0])
        df_fake = df_fake.sample(n=nsubs, random_state=0)
        df_test = df_test.sample(n=nsubs, random_state=0)
        df_train = df_train.sample(n=nsubs, random_state=0)

        # Check that this worked and that we have the same class distribution in each thing
        print(df_fake.shape)
        print(df_test.shape)
        print(df_train.shape)
        # far fewer examples of fraud in the fake data this is not good
        print(df_fake["BL"].astype(str).value_counts(normalize=True))
        print(df_test["BL"].astype(str).value_counts(normalize=True))
        print(df_train["BL"].astype(str).value_counts(normalize=True))

        cols_dict = {'Origin_Country': 'cat',
                     'Destination_Country': 'cat',
                     'OfficeIdCountry': 'cat',
                     'SaturdayStay': 'cat',
                     'PurchaseAnticipation': 'num',
                     'NumberPassenger': 'cat',
                     'StayDuration': 'num',
                     'Title': 'cat',
                     'TravelingWithChild': 'cat',
                     'ageAtPnrCreation': 'num',
                     'Nationality2': 'cat'}

        data_frames = [df_fake, df_test, df_train]

        randomState = 5
        np.random.seed(randomState)

        df = pd.concat((df_fake,df_test,df_train), axis=0, ignore_index=True)

        # Binary-encoding of labelled data in 'type'
        df.loc[df.BL == 'B', 'BL'] = 0
        df.loc[df.BL == 'L', 'BL'] = 1
        df.BL = df.BL.astype(int)  # convert dtype('O') to dtype(int)
        col_target = 'BL'
        cols_toOneHot = [c for c in cols_dict if cols_dict[c] == 'cat']
        cols_featuresNum = [c for c in cols_dict if cols_dict[c] == 'num']

        data_full, target_full, numEnc, le_list_OH, oneHotEnc, colsOH, le_target = preprocessData_model(df,
                                                                                                        cols_featuresNum,
                                                                                                        cols_toOneHot,
                                                                                                        col_target)

        # Do some horribly messy slicing to get some reasonable data frames
        df_fake = data_full[0:df_fake.shape[0],:]
        target_fake = target_full[0:df_fake.shape[0]]
        df_test = data_full[df_fake.shape[0]:df_fake.shape[0]+df_test.shape[0],:]
        target_test = target_full[df_fake.shape[0]:df_fake.shape[0]+df_test.shape[0]]
        df_train = data_full[df_fake.shape[0]:df_fake.shape[0]+df_test.shape[0]:,:]
        target_train = target_full[df_fake.shape[0]:df_fake.shape[0]+df_test.shape[0]:]


        # Check that this worked and that we have the same class distribution in each thing


        print(df_fake.shape)
        print(df_test.shape)
        print(df_train.shape)

        # Now we need a sweet flag situation
        if test_type == "GAN_train":

            training_data = df_fake
            training_target = target_fake
            test_data = df_test
            test_target = target_test

        elif test_type == "GAN_test":

            training_data = df_train
            training_target = target_train
            test_data = df_fake
            test_target = target_fake

        else:

            training_data = df_train
            training_target = target_train
            test_data = df_test
            test_target = target_test

        weights = (target_train == 0).sum() / (1.0 * (target_train == 1).sum())
        # # Long computation in this cell (~1.8 minutes)
        clf0 = XGBClassifier(max_depth=3, scale_pos_weight=weights, n_jobs=4)
        clf1 = RandomForestClassifier(random_state=0, n_estimators=100)
        clf2 = RandomForestClassifier(random_state=0, n_estimators=50)
        clf3 = RandomForestClassifier(random_state=0, n_estimators=1)
        clf4 = clf = DecisionTreeClassifier(max_depth=10, max_features='auto', random_state=0)  # ,min_samples_leaf=10)
        clf5 = LogisticRegression(random_state=0)

        clf_list = [clf0, clf1, clf2, clf3, clf4, clf5]
        clf_list.reverse()

        AUPRCs = []
        if all:
            for clf_i in clf_list:
                print("Training {}".format(clf_i))
                clf = clf_i
                trainY = training_target
                trainX = training_data
                testY = test_target
                testX = test_data


                probabilities = clf.fit(trainX, trainY).predict_proba(testX)
                AUPRC = average_precision_score(testY, probabilities[:, 1])
                AUPRCs.append(AUPRC)
                print('AUPRC = {}'.format(AUPRC))
                target_pred = clf.predict(testX)
                print("The area under the ROC curve is" + str(auroc(testY, target_pred)))
                print(np.mean(target_pred))
                print(np.sum(target_pred))
                print(classification_report(testY, target_pred))
                print(confusion_matrix(testY, target_pred))

        else:

            clf = XGBClassifier(max_depth=3, scale_pos_weight=weights, n_jobs=4)
            trainY = training_target
            trainX = training_data
            testY = test_target
            testX = test_data

            probabilities = clf.fit(trainX, trainY).predict_proba(testX)
            print('AUPRC = {}'.format(average_precision_score(testY, probabilities[:, 1])))

            target_pred = clf.predict(testX)
            print("The area under the ROC curve is" + str(auroc(testY, target_pred)))
            print(np.mean(target_pred))
            print(np.sum(target_pred))
            print(classification_report(testY, target_pred))
            print(confusion_matrix(testY, target_pred))

    elif use_case == 'pca_credit':

        which_fake_data = which_fake_data
        which_test_data = "data/pca_credit/test_evaluation.csv"
        which_train_data = "data/pca_credit/train_evaluation.csv"

        fake_data_path = which_fake_data
        test_data_path = os.path.join(root_path, which_test_data)
        train_data_path = os.path.join(root_path, which_train_data)

        df_fake = pd.read_csv(fake_data_path)
        df_test = pd.read_csv(test_data_path)
        df_train = pd.read_csv(train_data_path)

        print(df_fake.shape)
        print(df_test.shape)
        print(df_train.shape)

        # sample a subset of the generated data so that it is the same size as the test set
        nsubs = min(df_fake.shape[0], df_test.shape[0])
        df_fake = df_fake.sample(n=nsubs, random_state=0)
        df_test = df_test.sample(n=nsubs, random_state=0)
        df_train = df_train.sample(n=nsubs, random_state=0)

        # Check that this worked and that we have the same class distribution in each thing
        print(df_fake.shape)
        print(df_test.shape)
        print(df_train.shape)
        # far fewer examples of fraud in the fake data this is not good
        print(df_fake["Class"].astype(str).value_counts(normalize=True))
        print(df_test["Class"].astype(str).value_counts(normalize=True))
        print(df_train["Class"].astype(str).value_counts(normalize=True))

        cols_dict = {'Time': 'num', 'V1': 'num', 'V2': 'num', 'V3': 'num', 'V4': 'num', 'V5': 'num', 'V6': 'num', 'V7': 'num',
         'V8': 'num', 'V9': 'num', 'V10': 'num', 'V11': 'num', 'V12': 'num', 'V13': 'num', 'V14': 'num', 'V15': 'num',
         'V16': 'num', 'V17': 'num', 'V18': 'num', 'V19': 'num', 'V20': 'num', 'V21': 'num', 'V22': 'num', 'V23': 'num',
         'V24': 'num', 'V25': 'num', 'V26': 'num', 'V27': 'num', 'V28': 'num', 'Amount': 'num', 'Class': 'cat'}

        x_cols = {'Time': 'num', 'V1': 'num', 'V2': 'num', 'V3': 'num', 'V4': 'num', 'V5': 'num', 'V6': 'num', 'V7': 'num',
         'V8': 'num', 'V9': 'num', 'V10': 'num', 'V11': 'num', 'V12': 'num', 'V13': 'num', 'V14': 'num', 'V15': 'num',
         'V16': 'num', 'V17': 'num', 'V18': 'num', 'V19': 'num', 'V20': 'num', 'V21': 'num', 'V22': 'num', 'V23': 'num',
         'V24': 'num', 'V25': 'num', 'V26': 'num', 'V27': 'num', 'V28': 'num', 'Amount': 'num'}
        data_frames = [df_fake, df_test, df_train]

        # randomState = 5
        # np.random.seed(randomState)
        #
        # df = pd.concat((df_fake, df_test, df_train), axis=0, ignore_index=True)
        #
        # # Binary-encoding of labelled data in 'type'
        # df.loc[df.BL == 'B', 'BL'] = 0
        # df.loc[df.BL == 'L', 'BL'] = 1
        # df.BL = df.BL.astype(int)  # convert dtype('O') to dtype(int)
        # col_target = 'BL'
        # cols_toOneHot = [c for c in cols_dict if cols_dict[c] == 'cat']
        # cols_featuresNum = [c for c in cols_dict if cols_dict[c] == 'num']
        #
        # data_full, target_full, numEnc, le_list_OH, oneHotEnc, colsOH, le_target = preprocessData_model(df,
        #                                                                                                 cols_featuresNum,
        #                                                                                                 cols_toOneHot,
        #                                                                                                 col_target)

        # Do some horribly messy slicing to get some reasonable data frames
        target_fake = df_fake["Class"]
        df_fake = df_fake[x_cols]
        target_test = df_test["Class"]
        df_test = df_test[x_cols]
        target_train = df_train["Class"]
        df_train = df_train[x_cols]


        # Check that this worked and that we have the same class distribution in each thing

        print(df_fake.shape)
        print(df_test.shape)
        print(df_train.shape)

        # Now we need a sweet flag situation
        if test_type == "GAN_train":

            training_data = df_fake
            training_target = target_fake
            test_data = df_test
            test_target = target_test

        elif test_type == "GAN_test":

            training_data = df_train
            training_target = target_train
            test_data = df_fake
            test_target = target_fake

        else:

            training_data = df_train
            training_target = target_train
            test_data = df_test
            test_target = target_test

        weights = (target_train == 0).sum() / (1.0 * (target_train == 1).sum())
        # # Long computation in this cell (~1.8 minutes)
        clf0 = XGBClassifier(max_depth=3, scale_pos_weight=weights, n_jobs=4)
        clf1 = RandomForestClassifier(random_state=0, n_estimators=100)
        clf2 = RandomForestClassifier(random_state=0, n_estimators=50)
        clf3 = RandomForestClassifier(random_state=0, n_estimators=1)
        clf4  = DecisionTreeClassifier(max_depth=10, max_features='auto', random_state=0)  # ,min_samples_leaf=10)
        clf5 = LogisticRegression(random_state=0)

        clf_list = [clf0, clf1, clf2, clf3, clf4, clf5]
        clf_list.reverse()

        AUPRCs = []
        if all:
            for clf_i in clf_list:
                print("Training {}".format(clf_i))
                clf = clf_i
                trainY = training_target
                trainX = training_data
                testY = test_target
                testX = test_data

                probabilities = clf.fit(trainX, trainY).predict_proba(testX)
                AUPRC = average_precision_score(testY, probabilities[:, 1])
                AUPRCs.append(AUPRC)
                print('AUPRC = {}'.format(AUPRC))
                target_pred = clf.predict(testX)
                print("The area under the ROC curve is" + str(auroc(testY, target_pred)))
                print(np.mean(target_pred))
                print(np.sum(target_pred))
                print(classification_report(testY, target_pred))
                print(confusion_matrix(testY, target_pred))

        else:

            clf = XGBClassifier(max_depth=3, scale_pos_weight=weights, n_jobs=4)
            trainY = training_target
            trainX = training_data
            testY = test_target
            testX = test_data

            probabilities = clf.fit(trainX, trainY).predict_proba(testX)
            print('AUPRC = {}'.format(average_precision_score(testY, probabilities[:, 1])))

            target_pred = clf.predict(testX)
            print("The area under the ROC curve is" + str(auroc(testY, target_pred)))
            print(np.mean(target_pred))
            print(np.sum(target_pred))
            print(classification_report(testY, target_pred))
            print(confusion_matrix(testY, target_pred))





    elif use_case == "PAYSIM":

        which_fake_data = which_fake_data
        if raw:
            which_test_data = "data/Paysim_raw/test_evaluation.csv"
            which_train_data= "data/Paysim_raw/train_evaluation.csv"

        else:

            which_test_data = "data/Paysim/test_evaluation.csv"
            which_train_data= "data/Paysim/train_evaluation.csv"


        fake_data_path = which_fake_data
        test_data_path = os.path.join(root_path, which_test_data)
        train_data_path = os.path.join(root_path,which_train_data)

        df_fake = pd.read_csv(fake_data_path)
        df_test = pd.read_csv(test_data_path)
        df_train = pd.read_csv(train_data_path)

        print(df_fake.shape)
        print(df_test.shape)
        print(df_test.shape)

        #sample a subset of the generated data so that it is the same size as the test set
        nsubs = min(df_fake.shape[0], df_test.shape[0])
        df_fake = df_fake.sample(n=nsubs, random_state=0)
        df_test = df_test.sample(n=nsubs, random_state=0)
        df_train = df_train.sample(n=nsubs, random_state=0)

        # Check that this worked and that we have the same class distribution in each thing
        print(df_fake.shape)
        print(df_test.shape)
        print(df_train.shape)

        print(df_fake["isFraud"].astype(str).value_counts(normalize=True))
        print(df_test["isFraud"].astype(str).value_counts(normalize=True))
        print(df_train["isFraud"].astype(str).value_counts(normalize=True))

        data_frames = [df_fake,df_test,df_train]
        df_list = []
        for i,df_iter in enumerate(data_frames):

            df = df_iter.copy()
            df = df.loc[(df.type == 'TRANSFER') | (df.type == 'CASH_OUT')]

            randomState = 5
            np.random.seed(randomState)

            #X = X.loc[np.random.choice(X.index, 100000, replace = False)]


            # Eliminate columns shown to be irrelevant for analysis in the EDA
            df = df.drop(['isFlaggedFraud'], axis = 1)

            # Binary-encoding of labelled data in 'type'
            df.loc[df.type == 'TRANSFER', 'type'] = 0
            df.loc[df.type == 'CASH_OUT', 'type'] = 1
            df.type = df.type.astype(int) # convert dtype('O') to dtype(int)
        # step,type,amount,oldbalanceOrg,newbalanceOrig,oldbalanceDest,newbalanceDest,isFraud,isFlaggedFraud
            if i>-1:
                df['errorBalanceOrig'] = df.newbalanceOrig + df.amount - df.oldbalanceOrg
                df['errorBalanceDest'] = df.oldbalanceDest + df.amount - df.newbalanceDest
            df_list.append(df)

        # Check that this worked and that we have the same class distribution in each thing
        df_fake,df_test,df_train =  df_list[0], df_list[1], df_list[2]
        print(df_fake.shape)
        print(df_test.shape)
        print(df_train.shape)

        # Now we need a sweet flag situation
        if test_type == "GAN_train":

            training_data = df_fake
            test_data = df_test

        elif test_type == "GAN_test":

            training_data = df_train
            test_data = df_fake

        else:

            training_data = df_train
            test_data = df_test



        # # Long computation in this cell (~1.8 minutes)
        weights = (training_data['isFraud'] == 0).sum() / (1.0 * (training_data['isFraud'] == 1).sum())
        clf0 = XGBClassifier(max_depth = 3, scale_pos_weight = weights, n_jobs = 4)
        clf1 = RandomForestClassifier(random_state=0, n_estimators=100)
        clf2 = RandomForestClassifier(random_state=0, n_estimators=50)
        clf3 = RandomForestClassifier(random_state=0, n_estimators=1)
        clf4 = DecisionTreeClassifier(max_depth=10, max_features='auto', random_state=0)  # ,min_samples_leaf=10)
        clf5 = LogisticRegression(random_state=0)

        clf_list = [clf0,clf1,clf2,clf3,clf4,clf5]
        clf_list.reverse()

        AUPRCs = []
        if all:
            for clf_i in clf_list:
                print("Training {}".format(clf_i))
                clf = clf_i
                trainY = training_data['isFraud']
                trainX = training_data.drop(['isFraud'], axis =1)
                testY = test_data['isFraud']
                testX = test_data.drop(['isFraud'],axis =1)

                testX = testX[['step', 'type', 'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest','errorBalanceOrig', 'errorBalanceDest']]
                trainX = trainX[['step', 'type', 'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'errorBalanceOrig', 'errorBalanceDest']]


                probabilities = clf.fit(trainX, trainY).predict_proba(testX)
                AUPRC = average_precision_score(testY,probabilities[:, 1])
                AUPRCs.append(AUPRC)
                print('AUPRC = {}'.format(AUPRC))

                target_pred = clf.predict(testX)
                print("The area under the ROC curve is" + str(auroc(testY, target_pred)))
                print(np.mean(target_pred))
                print(np.sum(target_pred))
                print(classification_report(testY, target_pred))
                print(confusion_matrix(testY, target_pred))

        else:

            clf = XGBClassifier(max_depth=3, scale_pos_weight=weights, n_jobs=4)
            trainY = training_data['isFraud']
            trainX = training_data.drop(['isFraud'], axis=1)
            testY = test_data['isFraud']
            testX = test_data.drop(['isFraud'], axis=1)

            testX = testX[
                ['step', 'type', 'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest',
                'errorBalanceOrig', 'errorBalanceDest']]
            trainX = trainX[
                ['step', 'type', 'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest',
                'errorBalanceOrig', 'errorBalanceDest']]

            probabilities = clf.fit(trainX, trainY).predict_proba(testX)
            print('AUPRC = {}'.format(average_precision_score(testY, probabilities[:, 1])))
            AUPRC = average_precision_score(testY, probabilities[:, 1])
            AUPRCs.append(AUPRC)

            target_pred = clf.predict(testX)
            print("The area under the ROC curve is"+str(auroc(testY, target_pred)))
            print(np.mean(target_pred))
            print(np.sum(testY))
            print(classification_report(testY, target_pred))
            print(confusion_matrix(testY, target_pred))




    return AUPRCs


if __name__ == '__main__':

    # no_cond = "../generated_data/cramer_PNR_epochs_800_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)13:34:52,16M/final_generated_data.csv"
    which_fake_data = "../generated_data/CondCramer_C_pca_credit_SMOTE_epochs_400_latentSize_10_learning rate_0.0001_genDims_(128,128)discDims(128,128)crossdims0_10:58:40,18M2019-08-18_genM_5/final_generated_data.csv"
    real_tests = main_run(which_fake_data=which_fake_data,all=False,test_type='real',use_case="pca_credit",raw=True)
    gan_tests = main_run(which_fake_data=which_fake_data,all=False,test_type='GAN_test',use_case="pca_credit",raw=True)
    gan_trains = main_run(which_fake_data=which_fake_data,all=False,test_type='GAN_train',use_case="pca_credit",raw=True)

    # multiple line plot
    # plt.plot(real_tests,color='red')
    # plt.plot(gan_tests,color='blue')
    # plt.plot(gan_trains,color='black')
    # # plt.plot(gan_tests2, color='magenta')
    #
    # plt.legend(['real', 'GAN_test','GAN_train'])
    # plt.show()
