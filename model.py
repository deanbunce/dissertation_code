###############


import tensorflow as tf
import math


##############


def xavier_init(size):
    in_dim = size[0]
    xavier_stddev = 1. / tf.sqrt(in_dim / 2.)
    return tf.random_normal(shape=size, stddev=xavier_stddev)


def leaky_relu(x, leak=0.2, name="lrelu"):
     with tf.variable_scope(name):
         f1 = 0.5 * (1 + leak)
         f2 = 0.5 * (1 - leak)
         return f1 * x + f2 * tf.abs(x)

# Does not get used anywhere, seemingly but could be useful later?
def Ramp(x):
    return tf.minimum(tf.maximum(0., x),1.)


##############


#generate G and D models
class GenDisc(object):
    
    def __init__(self,disc,dims, out_dims,dims_nCross=0, soh=False, ohFeatureIndices=None, catEmbeddingDims=None, numColsLen=None, reg_scale=None,
                 use_dropout_everywhere=False,keep_prob=False,useResNet=False,
                 nSchortcut=None):
        
        #generate G or D, depending on value of disc variable which is a boolean
        self.disc = disc
        if disc:
            self.name = 'discriminator'
        else:
            self.name = 'generator'

        self.dims = dims
        self.out_dims = out_dims#1 for wgan, bigger for cramer
        self.reg_scale = reg_scale # This is the lambda value for the regularizer
        self.dims_nCross = dims_nCross # Number of cross layers
        self.use_dropout_everywhere = use_dropout_everywhere # Dropout
        self.keep_prob = keep_prob # For dropout
        self.useResNet = useResNet # we will not use resnet I don't think
        self.nSchortcut = nSchortcut # Also to do with resnet
        self.soh = soh # Seemingly is not used
        self.ohFeatureIndices = ohFeatureIndices
        self.catEmbeddingDims = catEmbeddingDims
        self.numColsLen = numColsLen # This is the number of numeric columns in the data


    def __call__(self, inputs, reuse=True):

        # Scope is either generator or discriminator
        with tf.variable_scope(self.name, reuse=reuse) as scope:
            # Regularizer
            if self.reg_scale is not None:
                scope.set_regularizer(tf.contrib.layers.l2_regularizer(scale=self.reg_scale))
                
            # Gets the number of dimensions in the tensor
            ndim = inputs.get_shape().ndims
            # accesses the last dimension i.e. the width of the tensor
            input_width = inputs.get_shape()[ndim-1].value

            stddev = 1.0 / math.sqrt(input_width)
            norm = tf.truncated_normal_initializer(stddev=stddev)
            const = tf.constant_initializer(0.0)

            # For the discriminator model where we are using embeddings for categorical features
            if self.disc and self.catEmbeddingDims is not None:
                #assuming numeric variables are first
                num_inputs = inputs[:,:self.numColsLen]
                cat_inputs =  inputs[:,self.numColsLen:]                
                embedded_cat_inputs=[]

                start = 0
                for i,end in enumerate(self.ohFeatureIndices):    # This comes from the catencoder which belongs to the
                    # data on the next page
                    cat_i=cat_inputs[:, start:end]
                    shape = [cat_i.get_shape()[ndim-1].value, self.catEmbeddingDims[i]]
                    W = tf.get_variable("W"+str(i), shape=shape,
                                        initializer=tf.random_normal_initializer(stddev=xavier_init(shape)))                                        
                    embedded_cat_inputs.append(tf.matmul(cat_i,W))

                inputs = tf.concat(axis=1, values=[num_inputs] + embedded_cat_inputs)

            #cross-net layers
            tempVec = inputs
            input_widthCross=inputs.get_shape()[ndim-1].value
            for i in range(self.dims_nCross):
                with tf.variable_scope('crossLayer'+str(i)):
                    w = tf.get_variable(name='weights', shape=[input_widthCross], dtype=tf.float32, initializer=norm)
                    b = tf.get_variable(name='biases', shape=[input_widthCross], dtype=tf.float32, initializer=const)
                    # Cheeky outer product
                    x0xt = tf.expand_dims(inputs, -1) * tf.expand_dims(tempVec, -2)
                    # Efficient implementation of the product
                    out= tf.tensordot(x0xt, w, [[ndim-1], [0]]) + tempVec + b
               
                tempVec = out
                if self.use_dropout_everywhere:    
                    with tf.name_scope("dropout"+str(i)):
                        tempVec = tf.nn.dropout(tempVec, self.keep_prob)
                
                
                
            ##fc layers
            tempVec_fc = inputs
            depth=0
            shortcutInput = inputs
            shortcutDim = input_width
            print(self.dims)
            for i, dim in enumerate(self.dims):
                print("The i and the dims in this loop for {} are {} and {}".format(self.name,i,dim))
                out_hidden =tf.layers.dense(tempVec_fc, dim,
                                            kernel_initializer=tf.random_normal_initializer(stddev=xavier_init([tempVec_fc.get_shape()[ndim-1].value, dim])),
                                            bias_initializer=tf.constant_initializer())#no activation
                depth=depth+1

                if self.useResNet and depth==self.nSchortcut:
                    depth=0
                    if dim==shortcutDim:
                        tempVecResize=shortcutInput
                    else:
                        newDim=dim
                        #tempVecResize=self.resNet_resizeInput(shortcutInput,newDim,rezType="project",scope="ProjectD"+str(i),reuse_scope=reuse)
                        tempVecResize = 0

                    tempVec_fc = leaky_relu(out_hidden + tempVecResize)
                    if self.use_dropout_everywhere:    
                        with tf.name_scope("dropoutD"+str(i)):
                            tempVec_fc = tf.nn.dropout(tempVec_fc, self.keep_prob)
                    
                    shortcutInput=tempVec_fc
                    shortcutDim=dim
                    
                else:
                    tempVec_fc = leaky_relu(out_hidden)
                    if self.use_dropout_everywhere:    
                        with tf.name_scope("dropoutD"+str(i)):
                            tempVec_fc = tf.nn.dropout(tempVec_fc, self.keep_prob)
               
                
                
            if self.dims_nCross>0:

                cat_res=tf.concat([tempVec,tempVec_fc],axis=1)#concat by column
            else:
                cat_res=tempVec_fc
            
            output = tf.layers.dense(cat_res, self.out_dims)
            

            if self.disc or self.ohFeatureIndices is None:
                prob = tf.nn.sigmoid(output)
                
            else: #softmax on each categorical feature
                num_outputs = output[:,:self.numColsLen]
                cat_outputs = output[:,self.numColsLen:]
                #take softmax on groups of cols representing categoricals
                out_cat_list=[]
                start = 0
                for i,end in enumerate(self.ohFeatureIndices):
                    out_cat_list.append(tf.nn.softmax(cat_outputs[:,start:end]))
                    start = end

                # The probability distribtons
                prob=tf.concat(axis=1, values=[tf.nn.sigmoid(num_outputs)] + out_cat_list)

            # Output would be the logits of the last dense layer
            return prob,output
        


    @property
    def vars(self):
        return [var for var in tf.global_variables() if self.name in var.name]

