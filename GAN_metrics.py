# Imports
import time
import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import sklearn
import sklearn.ensemble.gradient_boosting as gbc
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
import scipy as sc

# We are now going to implemenet the following metrics on some dummy data sets to see the runtimes and whether
# they provide any useful insights into the differences between the data sets
# 1. Maximum mean discrepancy which uses a Gaussian Kernel
# 2. AM Distance which is about the entropy of the generated data
# 3. number of statistically different bins

def full_evaluation(real_data,fake_data):

    nrow, ncol = real_data.shape
    features = real_data.columns
    print(features)
    # For nearest neighbours we need to unscale the training data

    # Now we do some univariate distribution comparisons

    # univariate evaluation: age histogram
    x = real_data.Age.values
    y = fake_data.Age.values
    xweights = 100 * np.ones_like(x) / x.size
    yweights = 100 * np.ones_like(y) / y.size
    fig, ax = plt.subplots()
    ax.hist(x, weights=xweights, color='blue', alpha=0.5)
    ax.hist(y, weights=yweights, color='red', alpha=0.5)
    ax.set(title='Histogram Comparison', ylabel='% of Dataset in Bin')
    ax.margins(0.05)
    ax.set_ylim(bottom=0)
    #plt.show()

    # univariate evaluation: stay duration for buisnes/leasure travellers
    # buisines
    x_0 = real_data.EducationNum[real_data.Income == 0.0].values
    y_0 = fake_data.EducationNum[fake_data.Income == 0.0].values
    # leasure
    x_1 = real_data.EducationNum[real_data.Income == 1.0].values
    y_1 = fake_data.EducationNum[fake_data.Income == 1.0].values

    xweights = 100 * np.ones_like(x_0) / x_0.size
    yweights = 100 * np.ones_like(y_0) / y_0.size
    fig, ax = plt.subplots()
    ax.hist(x_0, weights=xweights, color='blue', alpha=0.5)
    ax.hist(y_0, weights=yweights, color='red', alpha=0.5)
    ax.set(title='Histogram Comparison', ylabel='% of Dataset Bin')
    ax.margins(0.05)
    ax.set_ylim(bottom=0)
    #plt.show()

    xweights = 100 * np.ones_like(x_1) / x_1.size
    yweights = 100 * np.ones_like(y_1) / y_1.size
    fig, ax = plt.subplots()
    ax.hist(x_1, weights=xweights, color='blue', alpha=0.5)
    ax.hist(y_1, weights=yweights, color='red', alpha=0.5)
    ax.set(title='Histogram Comparison', ylabel='% of Dataset in Bin')
    ax.margins(0.05)
    ax.set_ylim(bottom=0)
    #plt.show()

    # Now use a binary classifier to try and tell the difference between real and fake data

    real_y_data = np.ones([nrow, 1])
    fake_y_data = np.zeros_like(real_y_data)
    labels = np.concatenate([real_y_data, fake_y_data])

    data = pd.concat([real_data, fake_data])
    data['labels'] = labels

    # Get some training and test indices
    train, test = train_test_split(data, test_size=0.2)


    clf = RandomForestClassifier(random_state=0,n_estimators=100)
    clf.fit(train[features], train['labels'])

    preds = clf.predict(test[features])

    print(np.sum([preds.reshape([int(0.4 * nrow), 1]) == test['labels'].values.reshape(int(0.4 * nrow), 1)]) / len(
        preds))


saver = tf.train.Saver()


fake_encoded = pd.read_csv("../generated_data/fakeEncoded_iter200.csv.gz")
real_encoded = pd.read_csv("../generated_data/realEncoded_iter200.csv.gz")

with tf.Session() as sess:
    saver.restore(sess, restore_from_path)


# Name the features appropriately
features = ["V"+str(i) for i in range(NUM_FEATURES)]

# Getting data frames for everything that we need
d1 = pd.DataFrame(true_x_data,columns = features)
d0 = pd.DataFrame(false_x_data,columns = features)
data = pd.concat([d1,d0])
data['labels'] = labels




# Get some training and test indices
train, test = train_test_split(data, test_size=0.2)




if __name__ == "__main__":

    # First lets get our kernels cracking
    # d0 = np.sum(np.triu(np.exp(-sklearn.metrics.pairwise_distances(true_x_data, true_x_data, metric='euclidean')**2)))
    # d1 = np.sum(np.triu(np.exp(-sklearn.metrics.pairwise_distances(true_x_data, false_x_data, metric='euclidean')**2)))
    # d2 = np.sum(np.triu(np.exp(-sklearn.metrics.pairwise_distances(false_x_data, false_x_data, metric='euclidean')**2)))
    #
    # c = 1/sc.special.comb(NCLASS,2)
    # M = c*d0 - 2*c*d1 + c*d2
    #
    #

