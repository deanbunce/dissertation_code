#Script to run the GAN
import argparse
import matplotlib.pyplot as plt
import pandas as pd
import os
import logging_setup
import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions
from datetime import datetime
now = datetime.now()
# from vanillaGan import GAN
from random import shuffle
import time
from sklearn.preprocessing import MinMaxScaler
from GAN_metrics import full_evaluation

## Evaluation imports

from sklearn.model_selection import train_test_split
import sklearn
import sklearn.ensemble.gradient_boosting as gbc
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
import scipy as sc

################################################################################################################
########################### Set up the parser and the logger ###########################
################################################################################################################
parser = argparse.ArgumentParser()

# Architecture and training
parser.add_argument("--model_type", type=str, help="Which model we are going to use", default='Vanilla_GAN')
parser.add_argument("--data_set", type=str, help="Indication of which data set to use", default='trial')
parser.add_argument("--feature_cols_name", type=str, help="Indication of features used", default='numerical')
parser.add_argument("--evaluate_model", type=str, help="Whether or not the model should be evaluated", default='Yes')
parser.add_argument("--num_epochs", type=int, help="Number of passes over the data", default=1)
parser.add_argument("--batch_size", type=int, help="Minibatch size for training", default=64)
parser.add_argument("--latent_size", type=int, help="Size of the latent space from which noise is sampled", default=32)
parser.add_argument("--save_model", type=str, help="Should the model be saved", default="No")
parser.add_argument("--save_model_path", type=str, help="The path to which we save the model", default="Yes")
parser.add_argument("--train_model", type=str, help="Whether or not the model should be trained", default="Yes")

# Other considerations
parser.add_argument("--root_path", type=str, help="root path", default='../')
# Testing
parser.add_argument("--eval_data_set", type=str, help="OOS Data set", default='eval_numeric')
parser.add_argument("--eval_method", type=str, help="Testing the gan", default='classifiers')
# Saving and restoring models
parser.add_argument("--restore_model", type=str, help="Using a saved model vs a new one", default='No')
parser.add_argument("--restore_from_path", type=str, help="path to restore modes", default="")

# Adding the model folder as an argument
parser.add_argument("--model_folder",type = str, help = "Name of folder for saving the models", default = 'models')




# These are now global variables that will receive arguments from the parser
args = parser.parse_args()
# Now we assign all of the variables based on the parser
model_type = args.model_type
data_set = args.data_set
feature_cols_name = args.feature_cols_name
evaluate_model = args.evaluate_model
num_epochs = args.num_epochs
batch_size = args.batch_size
latent_size = args.latent_size
save_model = args.save_model
train_model = args.train_model
save_model_path = args.save_model_path
root_path = args.root_path
eval_data_set = args.eval_data_set
eval_method = args.eval_method
restore_model = args.restore_model
restore_from_path = args.restore_from_path
model_folder = args.model_folder


# Turning the values given by the parser into workable features of our model
if data_set == 'trial':
    data = pd.read_pickle('../data/trial_data.pkl')
if data_set == 'log_standard_trial':
    data = pd.read_pickle('../data/log_standard_trial.pkl')
if data_set == 'adult_census_numerical':
    data = pd.read_pickle('../data/adult_census_numerical.pkl')
    print(data.head())

else: raise ValueError("Dataset is incorrect")


# Get the length of the data and the number of features in this data set
n_data, num_features = data.shape

if feature_cols_name == 'numerical':
    cols = ['step',  'amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest', 'isFraud']
if feature_cols_name == 'adult_census_numerical':
    cols = ['Age', 'fnlwgt', 'EducationNum', "CapitalGain", "CapitalLoss", 'HoursPerWeek']
else: raise ValueError("Feature choice is not defined")

if evaluate_model == 'Yes':
    print("Model will be evaluated")

if data_set == 'trial':
    assert feature_cols_name in ['numerical']

if eval_data_set == 'eval_numeric':
    eval_data = pd.read_pickle('../data/eval_numeric_data.pkl')
    eval_size = eval_data.shape

if eval_data_set == 'log_standard_trial_eval':
    eval_data = pd.read_pickle('../data/log_standard_trial_eval.pkl')
    eval_size = eval_data.shape

if eval_data_set == 'adult_test_numerical':
    eval_data = pd.read_pickle('../data/adult_test_numerical.pkl')
    eval_size = eval_data.shape
    print(eval_data.head())



if train_model == "Yes":
    train_model = True
else:
    train_model = False
# Now we set up some housekeeping that is done using the logger

tensorboard_name = model_type + '_' + \
                   data_set + '_' +\
                   'Features_' + str(feature_cols_name) + '_' + 'eval_' + evaluate_model + '_'+ \
                   'epochs_'+str(num_epochs) + 'batch_size_'+str(batch_size)+\
                   'latentSize_'+str(latent_size) +'_evaluatedOn'+'_'+ eval_data_set+'_evalMethod_'+ eval_method +\
                   now.strftime("%H:%M:%S,%dM")

name_experiment = tensorboard_name
logger = logging_setup.setup_logger(name_experiment, logs_dir= os.path.join(root_path, 'logs/'), also_stdout=True)
logger.info('PARAMS :  ' + name_experiment)
logger.info('')
logger.info(args)

################################################################################################################
########################### Define the GAN objects ###########################
################################################################################################################

class GAN(object):

    def __init__(self,batch_size,latent_size,num_features):

        self.DISC_VARIABLE_SCOPE = 'discriminator'
        self.GEN_VARIABLE_SCOPE = 'generator'
        self.batch_size = batch_size
        self.latent_size = latent_size
        self.num_features = num_features

        self.construct_placeholders()
        self.construct_computation_graph()
        sess = tf.Session()
        self.sess = sess
        if evaluate_model == "Yes":

            self.construct_eval_graph_placeholders()
            self.construct_eval_graph()

    def construct_placeholders(self):

        self.noise_placeholder = tf.placeholder(tf.float32,
                                                shape=[self.batch_size,self.latent_size],
                                                name='noise_placeholder')
        self.input_placeholder = tf.placeholder(tf.float32,
                                                shape=[self.batch_size, self.num_features],
                                                name='input_placeholder')

    # Generator made up of fully connected layer- placed here to allow for sampling
    def generator(self,noise_in):

        input_size, input_dims = noise_in.shape

        with tf.variable_scope(self.GEN_VARIABLE_SCOPE, reuse=tf.AUTO_REUSE):
            h = tf.layers.dense(noise_in, 32, activation=tf.nn.relu)
            h1 = tf.layers.dense(h,16,activation=tf.nn.relu)
            h2 = tf.layers.dense(h1,8, activation=tf.nn.relu)
            h3 = tf.layers.dense(h2,16, activation=tf.nn.relu)
            generated_data = tf.layers.dense(h3, self.num_features, activation=tf.nn.relu)

        return generated_data


    def construct_eval_graph_placeholders(self):
        self.eval_noise_placeholder = tf.placeholder(tf.float32,
                                                     shape=[eval_size[0],self.latent_size],
                                                     name='eval_noise_placeholder')
        self.eval_real_data_placeholder = tf.placeholder(tf.float32,
                                                     shape=[eval_size[0],eval_size[1]],
                                                     name='eval_noise_placeholder')
    def construct_eval_graph(self):

        self.eval_generated_data = self.generator(self.eval_noise_placeholder)

    def discriminator(self, data):

        with tf.variable_scope(self.DISC_VARIABLE_SCOPE, reuse=tf.AUTO_REUSE):
            h = tf.layers.dense(data, 32, activation=tf.nn.relu)
            h1 = tf.layers.dense(h, 32, activation=tf.nn.relu)
            h2 = tf.layers.dense(h1, 32, activation=tf.nn.relu)
            h3 = tf.layers.dense(h2, 32, activation=tf.nn.relu)
            prediction = tf.layers.dense(h3, 1, activation=None)

        return prediction

    # Surrogate loss prevents saturating gradients early on in training
    def loss_functions(self, batch_in, samples):

        # Get logits that are produced by the discriminator
        probabilities_generated = self.discriminator(samples)
        probabilities_real = self.discriminator(batch_in)

        # Produce the labels that are associated with real and fake data that is being passed into the GAN
        off_labels = tf.zeros_like(probabilities_generated)
        on_labels = tf.ones_like(probabilities_real)

        # Loss for the discriminator if we use the surrogate loss function for the vanilla GAN
        real_loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=on_labels, logits=probabilities_real)
        generated_loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=off_labels, logits=probabilities_generated)
        discriminator_loss = real_loss + generated_loss
        discriminator_loss = tf.reduce_mean(discriminator_loss)

        # Loss for the generator
        generator_loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=on_labels, logits=probabilities_generated)
        generator_loss = tf.reduce_mean(generator_loss)

        return discriminator_loss, generator_loss

    def construct_computation_graph(self):

        # Disc made up of fully connected layers only

        samples = self.generator(self.noise_placeholder)
        self.discriminator_loss, self.generator_loss = self.loss_functions(self.input_placeholder, samples)

        # Choose adam for both optimizers the first time
        discriminator_optimizer = tf.train.AdamOptimizer(0.0001, beta1=0.5, beta2=0.9)
        generator_optimizer = tf.train.AdamOptimizer(0.0003, beta1=0.9, beta2=0.9)

        # Optimize the discriminator
        discriminator_vars = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.DISC_VARIABLE_SCOPE)
        logger.info("Discriminator variables {}".format(discriminator_vars))
        generator_vars = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.GEN_VARIABLE_SCOPE)
        logger.info("Generator variables {}".format(generator_vars))
        # Optimize the generator
        self.generator_update_op = generator_optimizer.minimize(
            self.generator_loss, var_list=generator_vars)

        self.discriminator_update_op = discriminator_optimizer.minimize(
            self.discriminator_loss, var_list=discriminator_vars)


    # Sample from an independent 0,1

    def prior(self,test = False):

        prior_mean = tf.zeros(shape=(self.batch_size, self.latent_size), dtype=tf.float32)
        prior_log_scale = tf.zeros(shape=(self.batch_size, self.latent_size), dtype=tf.float32)

        prior = tfd.Independent(distribution=tfd.Normal(
            loc=prior_mean, scale=tf.exp(prior_log_scale)),reinterpreted_batch_ndims=1)

        if test:
            prior_mean = tf.zeros(shape=(eval_size[0], self.latent_size), dtype=tf.float32)
            prior_log_scale = tf.zeros(shape=(eval_size[0], self.latent_size), dtype=tf.float32)

            prior = tfd.Independent(distribution=tfd.Normal(
                loc=prior_mean, scale=tf.exp(prior_log_scale)), reinterpreted_batch_ndims=1)

        return prior

    def make_batches(self, data,feature_list):

        N = len(data)
        i = 0
        list_of_batches = []
        while i < N:
            list_of_batches.append(self.get_next_batch(start=i, data=data,feature_list=feature_list))

            i += self.batch_size

        return list_of_batches

    def get_next_batch(self, start, data, feature_list='all'):

        end = min(len(data), start + self.batch_size)

        if start + self.batch_size <= end:
            new_batch_df = data.loc[start:start+self.batch_size-1, feature_list]
            new_batch = new_batch_df.values

        else:
            new_batch_df = data.loc[end-self.batch_size:end, feature_list]
            new_batch = new_batch_df.values

        return new_batch

    # Function to evaluate how realistic the generated data is

    def evaluate(self, eval_data=eval_data):

        # Generate fake data to be compared during the evaluation
        eval_noise_data_dbn = self.prior(test=True)
        eval_noise_data = self.sess.run(eval_noise_data_dbn.sample())

        # Make the generated data back over its previous support
        unscaled_generated_data = MinMaxScaler.inverse_transform(self.eval_generated_data)
        # Calculate the JS divergence between the real and the generated data
        js_divergence = tfp.vi.jensen_shannon(unscaled_generated_data, eval_data)

        feed_dict = {
            self.eval_noise_placeholder: eval_noise_data
        }

        _eval_generated_data,_js_divergence = self.sess.run([self.eval_generated_data,js_divergence],
                                                            feed_dict = feed_dict)


        # Store the real and fake data in a Data frame and compare the distributions using one of the metrics

        df_fake = pd.DataFrame(_eval_generated_data, columns=cols)
        df_real = pd.DataFrame(eval_data, columns=cols)

        print("The jensen-shannon divergence after is {}".format(_js_divergence))


        return df_fake,df_real

    def train(self, data, num_epochs, feature_list, restore_model=False, save_model=False,
              restore_from_path=None, save_model_path=None):

        gen_losses = []
        disc_losses = []
        logging_freq = 50

        saver = tf.train.Saver()

        list_of_batches = self.make_batches(data, feature_list)

        number_of_batches = len(list_of_batches)
        n = len(data)

        # saving models
        if restore_model  == "Yes":
            saver.restore(self.sess, restore_from_path)
            logger.info("Successfully restored model from {}".format(restore_from_path))
        else:
            self.sess.run(tf.global_variables_initializer())
            logger.info("Initialized Global Variables")

        # TODO: Understand where this fits in and if it is needed
        if train_model:
            # sess.graph.finalize()
            epoch = -1

            while epoch < num_epochs:
                epoch += 1
                logger.info('Epoch: %d' % epoch)
                cur_total_time = 0
                shuffle(list_of_batches)
                i = -1
                while i < number_of_batches - 1:
                    i += 1

                    # Training
                    # Get the real data from the dataset passed in and sample the batches
                    real_data = list_of_batches[i]
                    noise_data_dbn = self.prior()
                    noise_data = self.sess.run(noise_data_dbn.sample())

                    feed_dict = {
                        self.input_placeholder: real_data,
                        self.noise_placeholder: noise_data
                    }

                    sess_time_start = time.time()

                    _discriminator_loss,_generator_loss,_,_ = \
                        self.sess.run([self.discriminator_loss, self.generator_loss, self.generator_update_op,
                                  self.discriminator_update_op], feed_dict=feed_dict)

                    cur_total_time += time.time() - sess_time_start

                    if i % logging_freq == 0:
                        time_per_data_point = cur_total_time / (logging_freq * self.batch_size)

                        logger.info('Num examples processed: %d. ; seconds per example: %.4f' % (
                            epoch * n + i * self.batch_size, time_per_data_point))
                        logger.info('Total time trained for: ')

                        logger.info("D loss {}, G loss {} and total time is {}".format(_discriminator_loss,_generator_loss,cur_total_time))
                        gen_losses.append(_generator_loss)
                        disc_losses.append(_discriminator_loss)


                        if save_model == "Yes":
                            saver.save(self.sess, '%s_epoch_%d_it_%d.ckpt' % (save_model_path, epoch, i))
                            logger.info('Stored the model')

                # Check the progress model during training

                if epoch == 0 or epoch == num_epochs//2:

                    self.evaluate(eval_data)

                    print("We would evaluate at epoch {}".format(epoch))

            # Final bit of evaluation
            self.evaluate(eval_data)
            logger.info("FINISHED TRAINING")
        return gen_losses,disc_losses

################################################################################################################
########################### Define the run function ###########################
################################################################################################################

def run():

    if model_type == 'Vanilla_GAN':
        model = GAN(batch_size, latent_size,num_features)
    else:
        raise ValueError("Model is incorrect")

    save_model_path = os.path.join(root_path, model_folder + "/" + name_experiment)

    gen_loss, disc_loss = model.train(data=data,
                                      num_epochs=num_epochs,
                                      feature_list=cols,
                                      save_model=save_model,
                                      save_model_path = save_model_path,
                                      restore_model = restore_model,
                                      restore_from_path = restore_from_path)

    df_fake,df_real = model.evaluate(eval_data)
    print(df_fake.head())
    print(df_real.head())

    full_evaluation(df_fake,df_real)


if __name__ == '__main__':
    run()
