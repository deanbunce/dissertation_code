
import seaborn as sns
import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

root_path = '../'
use_case = "PAYSIM"

df = pd.read_csv(os.path.join(root_path,'data/Paysim_raw/test.csv')) #Dataset Paysim_features
save_results = False


if use_case == "PAYSIM":
    cols_dict = {'step': 'num',
                 'type': 'cat',
                 'amount': 'num',
                 'oldbalanceOrg': 'num',
                 'newbalanceOrig': 'num',
                 'oldbalanceDest': 'num',
                 'newbalanceDest': 'num',
                 'isFraud': 'cat',
                 'isFlaggedFraud': 'cat'}
else:

    cols_dict = {'Origin_Country': 'cat',
                 'Destination_Country': 'cat',
                 'OfficeIdCountry': 'cat',
                 'SaturdayStay': 'cat',
                 'PurchaseAnticipation': 'num',
                 'NumberPassenger': 'cat',
                 'StayDuration': 'num',
                 'Title': 'cat',
                 'TravelingWithChild': 'cat',
                 'ageAtPnrCreation': 'num',
                 'Nationality2': 'cat',
                 'BL': 'cat'}


use_cols = [item for item in cols_dict]
cat_cols = [item for item in cols_dict if cols_dict[item] == 'cat']
num_cols = [item for item in cols_dict if cols_dict[item] == 'num']


# log_cols = ['amount', 'oldbalanceOrg', 'newbalanceOrig', 'oldbalanceDest', 'newbalanceDest']
#
# for i, name in enumerate(log_cols):
#     d0 = np.log10(df[name].values + 1)
#     # d0 = np.log1p( data['Amount'].values ) / np.log(10)
#     df[name] = d0


for col in num_cols:

    df = df.loc[df[col] != 0,:]


print(df.shape)
print(df.head())

print("The shape of the fraud data of this kind is {}".format(df.loc[df.isFraud == 1,:].shape))


# Create a big plot for the numerical columns
subs = len(num_cols)
cols = max(int(np.floor(np.sqrt(subs))), 3)
rows = min(cols + 1, 2)
if use_case == "PAYSIM":
    rows = 3
    cols = 3


fig, axs = plt.subplots(nrows=rows, ncols=cols, figsize=(rows * 5, cols * 5))
for i, c in enumerate(num_cols):
    x = np.log10(df.loc[df.isFraud == 1, c].values+1)
    y = np.log10(df.loc[df.isFraud == 0, c].values+1)
    if c != 'type':
        xweights = 100 * np.ones_like(x) / x.size
        yweights = 100 * np.ones_like(y) / y.size
        axs[i // cols, i % cols].hist(x, weights=xweights, color='red', alpha=0.5, bins=50)
        axs[i // cols, i % cols].hist(y, weights=yweights, color='grey', alpha=0.5, bins=50)
        axs[i // cols, i % cols].set(title='{}'.format(c), ylabel='% of Dataset in Bin')
        axs[i // cols, i % cols].margins(0.05)
        axs[i // cols, i % cols].set_ylim(bottom=0)

    else:

        axs[i // cols, i % cols].hist([df.loc[df.isFraud == 0, 'type'], df.loc[df.isFraud == 1, 'type']],
                 label=['normal', 'fraud'], color = ['grey','red'], alpha = 0.5,
                 normed=True)
        axs[i // cols, i % cols].set(title='{}'.format(c), ylabel='% of Dataset in Bin')
        axs[i // cols, i % cols].margins(0.05)
        axs[i // cols, i % cols].set_ylim(bottom=0)


axs[0, 0].legend(["fraudulant", "normal"])


fname_fig = os.path.join(root_path, "plots", "{}.png".format('initial data analysis'))

if save_results:
    plt.savefig(fname_fig, bbox_inches='tight')

else:
    plt.show()