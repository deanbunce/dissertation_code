# GAN Object
import tensorflow as tf
import tensorflow_probability as tfp
tfd = tfp.distributions
from datetime import datetime
now = datetime.now()
from random import shuffle
import time
import pandas as pd
################################################################################################################
########################### Define the GAN objects ###########################
################################################################################################################

class GAN(object):

    def __init__(self,batch_size,latent_size,num_features):

        self.DISC_VARIABLE_SCOPE = 'discriminator'
        self.GEN_VARIABLE_SCOPE = 'generator'
        self.batch_size = batch_size
        self.latent_size = latent_size
        self.construct_placeholders()
        self.construct_computation_graph()
        sess = tf.Session()
        self.sess = sess
        self.num_features = num_features
        if evaluate_model == "Yes":

            self.construct_eval_graph_placeholders()
            self.construct_eval_graph()


    def construct_placeholders(self):

        self.noise_placeholder = tf.placeholder(tf.float32,
                                                shape=[self.batch_size,self.latent_size],
                                                name='noise_placeholder')
        self.input_placeholder = tf.placeholder(tf.float32,shape=[self.batch_size, self.num_features], name='input_placeholder')

    # Generator made up of fully connected layer- placed here to allow for sampling
    def generator(self,noise_in):

        input_size, input_dims = noise_in.shape

        with tf.variable_scope(self.GEN_VARIABLE_SCOPE, reuse=tf.AUTO_REUSE):
            h = tf.layers.dense(noise_in, 32, activation=tf.nn.relu)
            h1 = tf.layers.dense(h,16,activation=tf.nn.relu)
            h2 = tf.layers.dense(h1,8, activation=tf.nn.relu)
            h3 = tf.layers.dense(h2,16, activation=tf.nn.relu)
            generated_data = tf.layers.dense(h3, self.num_features, activation=tf.nn.relu)

        return generated_data


    def construct_eval_graph_placeholders(self):
        self.eval_noise_placeholder = tf.placeholder(tf.float32,
                                                     shape=[eval_size[0],self.latent_size],
                                                     name='eval_noise_placeholder')
    def construct_eval_graph(self):

        self.eval_generated_data = self.generator(self.eval_noise_placeholder)


    def construct_computation_graph(self):

        # Disc made up of fully connected layers only
        def discriminator(data):

            with tf.variable_scope(self.DISC_VARIABLE_SCOPE, reuse=tf.AUTO_REUSE):
                h = tf.layers.dense(data, 32, activation=tf.nn.relu)
                h1 = tf.layers.dense(h,32,activation=tf.nn.relu)
                h2 = tf.layers.dense(h1,32, activation=tf.nn.relu)
                h3 = tf.layers.dense(h2,32, activation=tf.nn.relu)
                prediction = tf.layers.dense(h3, 1, activation=None)

            return prediction


        # Surrogate loss prevents saturating gradients early on in training
        def loss_functions(batch_in, samples):

            # Get logits that are produced by the discriminator
            probabilities_generated = discriminator(samples)
            probabilities_real = discriminator(batch_in)

            # Produce the labels that are associated with real and fake data that is being passed into the GAN
            off_labels = tf.zeros_like(probabilities_generated)
            on_labels = tf.ones_like(probabilities_real)

            # Loss for the discriminator if we use the surrogate loss function for the vanilla GAN
            real_loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=on_labels, logits=probabilities_real)
            generated_loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=off_labels, logits=probabilities_generated)
            discriminator_loss = real_loss + generated_loss
            discriminator_loss = tf.reduce_mean(discriminator_loss)

            # Loss for the generator
            generator_loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=on_labels, logits=probabilities_generated)
            generator_loss = tf.reduce_mean(generator_loss)

            return discriminator_loss, generator_loss


        # This is now the construction of the graph, the important things that we want to keep from the graph
        # Are given the "self" prefix as they belong to the object. The others are just methods defined within the object
        # This should still work for model saving
        samples = self.generator(self.noise_placeholder)
        self.discriminator_loss, self.generator_loss = loss_functions(self.input_placeholder, samples)

        # Choose adam for both optimizers the first time
        discriminator_optimizer = tf.train.AdamOptimizer(0.0001, beta1=0.5, beta2=0.9)
        generator_optimizer = tf.train.AdamOptimizer(0.0003, beta1=0.9, beta2=0.9)

        # Optimize the discriminator
        discriminator_vars = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.DISC_VARIABLE_SCOPE)
        logger.info("Discriminator variables {}".format(discriminator_vars))
        generator_vars = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES, scope=self.GEN_VARIABLE_SCOPE)
        logger.info("Generator variables {}".format(generator_vars))
        # Optimize the generator
        self.generator_update_op = generator_optimizer.minimize(
            self.generator_loss, var_list=generator_vars)

        self.discriminator_update_op = discriminator_optimizer.minimize(
            self.discriminator_loss, var_list=discriminator_vars)


    # These functions provide what is needed for the feed_dict during training

    # Sample from an independent 0,1

    def prior(self,test = False):

        prior_mean = tf.zeros(shape=(self.batch_size, self.latent_size), dtype=tf.float32)
        prior_log_scale = tf.zeros(shape=(self.batch_size, self.latent_size), dtype=tf.float32)

        prior = tfd.Independent(distribution=tfd.Normal(
            loc=prior_mean, scale=tf.exp(prior_log_scale)),reinterpreted_batch_ndims=1)

        if test:
            prior_mean = tf.zeros(shape=(eval_size[0], self.latent_size), dtype=tf.float32)
            prior_log_scale = tf.zeros(shape=(eval_size[0], self.latent_size), dtype=tf.float32)

            prior = tfd.Independent(distribution=tfd.Normal(
                loc=prior_mean, scale=tf.exp(prior_log_scale)), reinterpreted_batch_ndims=1)

        return prior

    def make_batches(self, data,feature_list):

        N = len(data)
        i = 0
        list_of_batches = []
        while i < N:
            list_of_batches.append(self.get_next_batch(start=i, data=data,feature_list=feature_list))

            i += self.batch_size

        return list_of_batches

    def get_next_batch(self, start, data, feature_list='all'):

        end = min(len(data), start + self.batch_size)

        if start + self.batch_size <= end:
            new_batch_df = data.loc[start:start+self.batch_size-1, feature_list]
            new_batch = new_batch_df.values

        else:
            new_batch_df = data.loc[end-self.batch_size:end, feature_list]
            new_batch = new_batch_df.values

        return new_batch

    def train(self, data, num_epochs, feature_list, restore_model=False, save_model=False,
              restore_from_path=None, save_model_path=None):


        gen_losses = []
        disc_losses = []
        LOGGING_FREQ = 100

        saver = tf.train.Saver()

        list_of_batches = self.make_batches(data, feature_list)

        number_of_batches = len(list_of_batches)
        N = len(data)

        # This will come in when we start saving models
        if restore_model  == "Yes":
            saver.restore(self.sess, restore_from_path)
            logger.info("Successfully restored model from {}".format(restore_from_path))
        else:
            self.sess.run(tf.global_variables_initializer())
            logger.info("Initialized Global Variables")

        # TODO: Understand where this fits in and if it is needed
        if train_model:
            # sess.graph.finalize()
            epoch = -1

            while epoch < num_epochs:
                epoch += 1
                logger.info('Epoch: %d' % epoch)
                #print(time.time())
                cur_total_time = 0
                shuffle(list_of_batches)

                i = -1
                while i < number_of_batches - 1:
                    i += 1

                    # Training
                    # Get the real data from the dataset passed in and sample the batches
                    real_data = list_of_batches[i]
                    noise_data_dbn = self.prior()
                    noise_data = self.sess.run(noise_data_dbn.sample())

                    feed_dict = {
                        self.input_placeholder: real_data,
                        self.noise_placeholder: noise_data
                    }

                    sess_time_start = time.time()

                    _discriminator_loss,_generator_loss,_,_ = \
                        self.sess.run([self.discriminator_loss, self.generator_loss, self.generator_update_op,
                                  self.discriminator_update_op], feed_dict=feed_dict)

                    cur_total_time += time.time() - sess_time_start

                    if i % LOGGING_FREQ == 0:
                        time_per_data_point = cur_total_time / (LOGGING_FREQ * self.batch_size)

                        logger.info('Num examples processed: %d. ; seconds per example: %.4f' % (
                            epoch * N + i * self.batch_size, time_per_data_point))

                        logger.info("D loss {}, G loss {} and total time is {}".format(_discriminator_loss,_generator_loss,cur_total_time))
                        gen_losses.append(_generator_loss)
                        disc_losses.append(_discriminator_loss)

                        if save_model == "Yes":
                            saver.save(self.sess, '%s_epoch_%d_it_%d.ckpt' % (save_model_path, epoch, i))
                            logger.info('Stored the model')
            logger.info("FINISHED TRAINING")
        return gen_losses,disc_losses

    # Function to evaluate how realistic the generated data is

    def evaluate(self,eval_data = eval_data):

        # Generate fake data to be compared durig the evaluation
        eval_noise_data_dbn = self.prior(test=True)
        eval_noise_data = self.sess.run(eval_noise_data_dbn.sample())

        feed_dict = {
            self.eval_noise_placeholder: eval_noise_data
        }

        _eval_generated_data = self.sess.run(self.eval_generated_data,feed_dict = feed_dict)

        # Store the real and fake data in a Data frame and compare the distributions using one of the metrics

        df_fake = pd.DataFrame(_eval_generated_data, columns=cols)
        df_real = pd.DataFrame(eval_data, columns=cols)

        # In this case we create a data frame and then return the correlations in each of them
        fake_correlations = df_fake.corr()
        real_correlations = df_real.corr()

        return fake_correlations,real_correlations,df_fake,df_real

